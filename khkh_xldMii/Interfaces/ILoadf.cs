using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace khkh_xldMii.Interfaces {
    public interface ILoadf {
        void LoadOf(int x, string fp);
        void SetJointOf(int x, int joint);
        void DoRecalc();
    }
}
