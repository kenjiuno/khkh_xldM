using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class FCurves {
        public int TargetBoneIndex { get; set; }
        public int Flags { get; set; }
        public int KeyCount { get; set; }
        public int KeyStartIndex { get; set; }
        public List<FCurvesKeyImmediate> Keys { get; set; } = new List<FCurvesKeyImmediate>();

        public static FCurves ReadFrom(BinaryReader reader) {
            var it = new FCurves();
            it.TargetBoneIndex = reader.ReadUInt16();
            it.Flags = reader.ReadByte();
            it.KeyCount = reader.ReadByte();
            it.KeyStartIndex = reader.ReadUInt16();

            return it;
        }

        public override string ToString() {
            return string.Format("{0:X2} {1:X2} {2:X2} {3:X2} {4:X4}", TargetBoneIndex, Flags, KeyCount, KeyStartIndex);
        }
    }
}
