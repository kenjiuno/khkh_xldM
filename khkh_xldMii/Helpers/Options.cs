using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace khkh_xldMii.Helpers {
    public class Options {
        public bool ShouldExportIndividualClips { get; set; }
        public bool ShouldShowNoesisConsole { get; set; }
        public bool JsonEmission { get; set; }
        public bool IkBones { get; set; }

        [YamlIgnore]
        [Option("enable-export-individual-clips", HelpText = "Export individual clips on ASET export -> on")]
        public bool EnableShouldExportIndividualClips { get; set; }

        [YamlIgnore]
        [Option("disable-export-individual-clips", HelpText = "Export individual clips on ASET export -> off")]
        public bool DisableShouldExportIndividualClips { get; set; }

        [YamlIgnore]
        [Option("enable-show-noesis-console", HelpText = "Show Noesis console on ASET export -> on")]
        public bool EnableShouldShowNoesisConsole { get; set; }

        [YamlIgnore]
        [Option("disable-show-noesis-console", HelpText = "Show Noesis console on ASET export -> off")]
        public bool DisableShouldShowNoesisConsole { get; set; }

        [YamlIgnore]
        [Option("enable-json-emission", HelpText = "Enable json emission")]
        public bool EnableJsonEmission { get; set; }

        [YamlIgnore]
        [Option("disable-json-emission", HelpText = "Disable json emission")]
        public bool DisableJsonEmission { get; set; }

        [YamlIgnore]
        [Option("include-ik-bones", HelpText = "Include IK bones")]
        public bool IncludeIkBones { get; set; }

        [YamlIgnore]
        [Option("exclude-ik-bones", HelpText = "Exclude IK bones")]
        public bool ExcludeIkBones { get; set; }

        [Option("noesis", HelpText = "Specify a full path to noesis.exe")]
        public string NoesisExecutable { get; set; }

        internal bool TryFindingNoesisExecutable(out string noesisExecutablePath) {
            noesisExecutablePath = NoesisExecutable;

            return true
                && NoesisExecutable != null
                && NoesisExecutable.Length != 0
                && File.Exists(NoesisExecutable);
        }
    }
}
