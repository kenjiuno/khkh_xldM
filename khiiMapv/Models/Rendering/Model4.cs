using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace khiiMapv.Models.Rendering {
    public class Model4 {
        public List<int> VifPacketIndexMap { get; set; }
        public List<int[]> VifPacketRenderingGroups { get; set; }
        public List<VifPacketPair> VifPacketPairs { get; set; } = new List<VifPacketPair>();
    }
}
