using NUnit.Framework;
using System.IO;
using System.Linq;

namespace LibMemoryBar.Tests {
    public class Class1 {
        private readonly MemoryBar _memoryBar = new MemoryBar();

        [Test]
        [Ignore("TDD")]
        public void P_EX100() {
            var entries = _memoryBar.Read(File.ReadAllBytes(@"H:\KH2fm.OpenKH\obj\P_EX100.mdlx"))
                .ToArray();
        }
    }
}