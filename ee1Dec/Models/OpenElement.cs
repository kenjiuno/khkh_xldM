using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ee1Dec.Models {
    public class OpenElement {
        [XmlAttribute("key")]
        public string prefix;

        [XmlAttribute("trac")]
        public string tracfp;

        [XmlAttribute("desc")]
        public string descfp;

        [XmlAttribute("dllDir")]
        public string dllDir;

        [XmlAttribute("eeramRanges")]
        public string eeramRanges;

        [XmlAttribute("eeramLzmaSaveTo")]
        public string eeramLzmaSaveTo;

        public OpenElement() {
        }

        public override string ToString() {
            return "Open " + prefix + ", " + Path.GetFileName(tracfp) + ", " + Path.GetFileName(descfp);
        }
    }
}
