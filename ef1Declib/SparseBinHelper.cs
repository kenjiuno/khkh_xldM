using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib {
    public class SparseBinHelper {
        public static void Decode(Stream inStream, Stream outStream) {
            var reader = new BinaryReader(inStream);
            while (true) {
                var first = reader.ReadInt32();
                var length = reader.ReadInt32();
                if (first == 0 && length == 0) {
                    break;
                }

                var block1Len = reader.ReadInt32();
                var nextPos = inStream.Position + block1Len;
                outStream.Position = first;
                using (var deflate = new DeflateStream(inStream, CompressionMode.Decompress, true)) {
                    var buffer = new byte[8192];
                    var rest = length;
                    while (1 <= rest) {
                        var read = deflate.Read(buffer, 0, Math.Min(8192, rest));
                        if (read == 0) {
                            throw new EndOfStreamException();
                        }
                        outStream.Write(buffer, 0, read);
                        rest -= read;
                    }
                }
                inStream.Position = nextPos;
            }
        }
    }
}
