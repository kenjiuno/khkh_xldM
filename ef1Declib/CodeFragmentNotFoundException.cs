using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib {
    public class CodeFragmentNotFoundException : Exception {
        public CodeFragmentNotFoundException(uint pc, string procedureHint) : base($"Code fragment {pc:X8} of {procedureHint} not found") {

        }
    }
}
