using hex04BinTrack;
using khkh_xldMii.V;
using NUnit.Framework;
using SlimDX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Mdlx2Blender249.Tests {
    public class Class1 {
        private string KH2DumpRoot => @"H:\KH2fm.OpenKH";
        private string ExportRoot => Path.Combine(TestContext.CurrentContext.WorkDirectory, "export");

        private static string Mkdir(string dir, params string[] paths) {
            var lastDir = Path.Combine(new string[] { dir }.Concat(paths).ToArray());
            Directory.CreateDirectory(lastDir);
            return lastDir;
        }

        [Test]
        public void P_EX110_WithMset() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\P_EX100.mdlx"),
                Path.Combine(KH2DumpRoot, @"obj\P_EX100.mset"),
                Path.Combine(Mkdir(ExportRoot, "P_EX110_WithMset"), @"model.py")
            );
        }

        [Test]
        public void P_EX100() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\P_EX100.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "P_EX100"), @"model.py")
            );
        }

        [Test]
        public void P_EX100_TR() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\P_EX100_TR.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "P_EX100_TR"), @"model.py")
            );
        }

        [Test]
        public void H_EX500_BTLF() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_EX500_BTLF.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_EX500_BTLF"), @"model.py")
            );
        }

        [Test]
        public void H_EX740() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_EX740.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_EX740"), @"model.py")
            );
        }

        [Test]
        public void H_ZZ010() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_ZZ010.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_ZZ010"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex2")]
        public void B_AL100_2ND() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\B_AL100_2ND.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "B_AL100_2ND"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex2")]
        public void N_EX550() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\N_EX550.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "N_EX550"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex3")]
        public void P_EX110_NPC_PAJAMAS() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\P_EX110_NPC_PAJAMAS.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "P_EX110_NPC_PAJAMAS"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex4")]
        public void F_TR570() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\F_TR570.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "F_TR570"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex5")]
        public void H_MU050() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_MU050.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_MU050"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex5")]
        public void H_AL030() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_AL030.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_AL030"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex6")]
        //[Category("Generate_0_0_0")] //fixed
        public void H_TR010() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_TR010.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_TR010"), @"model.py")
            );
        }

        [Test]
        [Category("MaxWeightsPerVertex7")]
        public void H_BB050_TSURU() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_BB050_TSURU.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_BB050_TSURU"), @"model.py")
            );
        }

        [Test]
        public void H_EX500() {
            new Program().Run(
                Path.Combine(KH2DumpRoot, @"obj\H_EX500.mdlx"),
                null,
                Path.Combine(Mkdir(ExportRoot, "H_EX500"), @"model.py")
            );
        }

        [Test]
        public void SrkDae2Mdlx_good() {
            new Program().Run(
                @"H:\Dev\KH2\SrkDae2Mdlx-latest\Sample\P_EX100-fromDAE-good.mdlx",
                null,
                Path.Combine(Mkdir(@"H:\Dev\KH2\SrkDae2Mdlx-latest\Sample", "good"), @"model.py")
            );
        }

        [Test]
        public void SrkDae2Mdlx_bad() {
            new Program().Run(
                @"H:\Dev\KH2\SrkDae2Mdlx-latest\Sample\P_EX100-fromDAE-bad.mdlx",
                null,
                Path.Combine(Mkdir(@"H:\Dev\KH2\SrkDae2Mdlx-latest\Sample", "bad"), @"model.py")
            );
        }

        [Test]
        public void MDLX_Import_bad() {
            new Program().Run(
                @"C:\KM\C\New.mdlx",
                null,
                Path.Combine(Mkdir(@"C:\KM\C"), @"model.py")
            );
        }

        [Test]
        public void HP1tri() {
            new Program().Run(
                @"H:\Dev\KH2\osdanova\HP1tri\HP1tri.mdlx",
                null,
                Path.Combine(Mkdir(@"H:\Dev\KH2\osdanova\HP1tri\export"), @"model.py")
            );
        }

        [Test]
        //[TestCase(@"H:\Dev\KH2\osdanova\Chest_vif1.model", @"H:\Dev\KH2\osdanova\Chest_vif1-layout.txt", 2)]
        [TestCase(@"H:\Dev\KH2\osdanova\HP_Sora_VIF1.model", @"H:\Dev\KH2\osdanova\HP_Sora_VIF1-layout.txt", 10)]
        public void Chest_vif1(string modelFile, string textFile, int matrixCount) {
            var vifbin = File.ReadAllBytes(modelFile);

            var mem = new VU1Mem();
            var tops = 0;// 0x40;
            new ParseVIF1(mem).Parse(new MemoryStream(vifbin, false), tops);
            var export = VU1MemoryExporter.Export(mem, tops, 0, new int[matrixCount], Matrix.Identity);
            var writer = new StringWriter();
            export.hexDump.WriteTo(writer);
            File.WriteAllText(textFile, writer.ToString());
        }

        [Test]
        public void P_EX100_out() {
            new Program().Run(
                @"H:\Dev\KH2\Mikote111\P_EX100.out.mdlx",
                null,
                Path.Combine(Mkdir(@"H:\Dev\KH2\Mikote111\P_EX100.out"), @"model.py")
            );
        }

        [Test]
        public void High_sora_singleweight() {
            new Program().Run(
                @"C:\KM\C\high_sora_singleweight.mdlx",
                null,
                Path.Combine(Mkdir(@"C:\KM\C\high_sora_singleweight.out"), @"model.py")
            );
        }

        [Test]
        public void High_sora_singleweight_v2() {
            new Program().Run(
                @"C:\KM\C\high_sora_singleweight.v2.mdlx",
                null,
                Path.Combine(Mkdir(@"C:\KM\C\high_sora_singleweight.v2.out"), @"model.py")
            );
        }

        [Test]
        public void Robo() {
            new Program().Run(
                @"H:\Dev\KH2\Mikote111\robo.mdlx",
                null,
                Path.Combine(Mkdir(@"H:\Dev\KH2\Mikote111\robo.export"), @"model.py")
            );
        }




    }
}
