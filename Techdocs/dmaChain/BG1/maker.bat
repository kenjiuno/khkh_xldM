npx bit-field -i map1.json --bits 128 --lanes 4 --hflip > map1.svg
npx bit-field -i map2.json --bits 16 --lanes 2 --hflip > map2.svg
npx bit-field -i map3.json --bits 32 --lanes 4 --hflip > map3.svg
