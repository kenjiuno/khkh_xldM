using khiiMapv.Models.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace khiiMapv {
    /// <summary>
    /// Draw source
    /// </summary>
    public class DSource {
        public String name;
        public Model4 mapObj;
        public ModelTextureSet texSet;
        public Parse4Mdlx mdlxObj;

        public bool initialVisible => (mapObj != null) ? (name == "MAP" || name.StartsWith("SK")) : true;
    }
}
