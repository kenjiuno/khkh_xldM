# BackgroundGroup

The bit field representation of BackgroundGroup implemented by osdanova: [screenshot](screenshot.png)

## Layout

<img src="map1.svg" style="background:white">

## Flags

<img src="map2.svg" style="background:white">

## Attributes

<img src="map3.svg" style="background:white">
