using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using CommandLine;
using khkh_xldMii.Helpers;
using khkh_xldMii.Properties;
using System.Linq;

namespace khkh_xldMii {
    static class Program {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static int Main(string[] args) {
            var helpWriter = new StringWriter();
            var parser = new Parser(
                parserSettings => {
                    parserSettings.HelpWriter = helpWriter;
                }
            );
            return parser.ParseArguments<Options>(args)
                .MapResult<Options, int>(
                    Run,
                    ex => {
                        if (Environment.UserInteractive) {
                            MessageBox.Show(
                                helpWriter.ToString(),
                                "khkh_xldMii",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error
                            );
                        }
                        return 1;
                    }
                );
        }

        private static int Run(Options arg) {
            Options config;
            if (File.Exists(ConfigFile)) {
                config = new YamlDotNet.Serialization.DeserializerBuilder()
                    .IgnoreUnmatchedProperties()
                    .Build()
                    .Deserialize<Options>(
                        File.ReadAllText(ConfigFile)
                    );

                #region Apply changes from command line

                config.ShouldExportIndividualClips =
                    arg.EnableShouldExportIndividualClips ? true
                    : arg.DisableShouldExportIndividualClips ? false
                    : config.ShouldExportIndividualClips;

                config.ShouldShowNoesisConsole =
                    arg.EnableShouldShowNoesisConsole ? true
                    : arg.DisableShouldShowNoesisConsole ? false
                    : config.ShouldShowNoesisConsole;

                config.JsonEmission =
                    arg.EnableJsonEmission ? true
                    : arg.DisableJsonEmission ? false
                    : config.JsonEmission;

                config.IkBones =
                    arg.IncludeIkBones ? true
                    : arg.ExcludeIkBones ? false
                    : config.IkBones;

                config.NoesisExecutable =
                    arg.NoesisExecutable
                    ?? config.NoesisExecutable
                    ?? "";

                #endregion
            }
            else {
                config = arg;
            }

            void SaveConfig() {
                File.WriteAllText(
                    ConfigFile,
                    new YamlDotNet.Serialization.SerializerBuilder()
                        .Build()
                        .Serialize(
                            config
                        )
                );
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormII(config, SaveConfig));
            return 0;
        }

        public static string ConfigFile => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config.yml");

        public static string BindPresetXmlFile => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BindPreset.xml");

        public static string SearchFoldersTxtFile => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SearchFolders.txt");
    }
}