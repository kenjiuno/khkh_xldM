using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace khiiMapv.Models.Rendering {
    public class ModelTextureSet {
        public Bitmap[] Pictures { get; set; }

        public ModelTextureSet(List<Bitmap> pics) {
            this.Pictures = pics.ToArray();
        }
    }
}
