using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace khkh_xldMii.Helpers {
    public class ExportRootMotion {
        [JsonProperty("numFk")] public int NumFk { get; set; }
        [JsonProperty("numIk")] public int NumIk { get; set; }
        [JsonProperty("rootPosition")] public RootPositionClass RootPosition { get; set; }

        [JsonProperty("fullBones")] public BoneClass[] FullBones { get; set; }

        public class BoneClass {
            [JsonProperty("index")] public int Index { get; set; }

            /// <summary>
            /// Bone index to parent bone. -1 if there is no parent.
            /// </summary>
            [JsonProperty("parent")] public int Parent { get; set; }
        }

        public class RootPositionClass {
            [JsonProperty("scalingX")] public float ScalingX { get; set; }
            [JsonProperty("scalingY")] public float ScalingY { get; set; }
            [JsonProperty("scalingZ")] public float ScalingZ { get; set; }
            [JsonProperty("rotationX")] public float RotationX { get; set; }
            [JsonProperty("rotationY")] public float RotationY { get; set; }
            [JsonProperty("rotationZ")] public float RotationZ { get; set; }
            [JsonProperty("translationX")] public float TranslationX { get; set; }
            [JsonProperty("translationY")] public float TranslationY { get; set; }
            [JsonProperty("translationZ")] public float TranslationZ { get; set; }
            [JsonProperty("fcurves")] public int[] FCurves { get; set; } = new int[9];
            [JsonProperty("fcurvesTargetBoneIndexList")] public int[] FCurvesTargetBoneIndexList { get; set; } = new int[9];
        }
    }
}
