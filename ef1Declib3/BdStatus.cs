using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib3 {
    public enum BdStatus {
        Exec = 0,
        Halt = 1,
        Exit = 2,
        Ret = 3,
        Ignore = 4,
        Error = 5,
    }
}
