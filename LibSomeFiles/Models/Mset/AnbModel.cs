using System;
using System.Collections.Generic;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class AnbModel {
        public List<InitialPosition> InitialPositionList { get; set; } = new List<InitialPosition>();
        public List<FCurves> FCurvesFKList { get; set; } = new List<FCurves>();
        public List<FCurves> FCurvesIKList { get; set; } = new List<FCurves>();
        public List<Constraint> ConstraintList { get; set; } = new List<Constraint>();
        public List<JointIndex> JointIndexList { get; set; } = new List<JointIndex>();
        public List<AxBone> IKBones { get; set; } = new List<AxBone>();
        public List<FCurvesKey> FCurvesKeysList { get; set; } = new List<FCurvesKey>();
        public List<float> KeyValueList { get; set; } = new List<float>();
        public List<float> FrameTimeList { get; set; } = new List<float>();
        public List<float> TangentValueList { get; set; } = new List<float>();
        public RootPosition RootPosition { get; set; }

        public int OffsetIKBones { get; set; } = 0;
        public int NumIKBones { get; set; } = 0;
    }
}
