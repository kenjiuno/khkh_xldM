using System;
using System.Collections.Generic;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class FCurvesKeyImmediate {
        public int AbsKeyStartIndex { get; set; }
        public float FrameTime { get; set; }
        public float KeyValue { get; set; }
        public float TangentLeft { get; set; }
        public float TangentRight { get; set; }

    }
}
