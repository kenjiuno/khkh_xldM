from invoke import task
import os
import shutil
import io

vcvars32Bat = "H:\\Program Files\\Microsoft Visual Studio\\2022\\Professional\\VC\\Auxiliary\\Build\\vcvars32.bat"


def deleteIfExists(file):
    if os.path.exists(file):
        os.unlink(file)


@task
def pack(c):
    c.run("call \"%s\" && msbuild khkh_xldM.sln /p:Configuration=Release /p:Platform=x86" % (vcvars32Bat))
    if os.path.exists("Release\\staging"):
        shutil.rmtree("Release\\staging")
    os.makedirs("Release\\staging")
    c.run("echo a|xcopy /d \"khkh_xldMii\\bin\\x86\\Release\" Release\\staging")
    c.run("echo a|xcopy /d \"khiiMapv\\bin\\x86\\Release\" Release\\staging")
    with io.StringIO() as s:
        c.run("git describe --tags", out_stream=s)
        tag = s.getvalue().strip()
        arcFile = "khkh_xldMii-%s.zip" % (tag,)
        deleteIfExists("Release\\%s" % (arcFile,))
        with c.cd("Release\\staging"):
            c.run("\"C:\\Program Files\\7-Zip\\7z.exe\" a ..\\%s ." %
                  (arcFile,), echo=True)
