using ee1Dec.C;
using ef1Declib3;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib3Tests {
    public class Class1 {
        private readonly string _scriptsDir = Path.Combine(
            TestContext.CurrentContext.WorkDirectory, "..", "..", "Scripts"
        );

        [Test]
        [TestCase(10001, BdStatus.Ret, null)]
        [TestCase(10002, BdStatus.Halt, null)]
        [TestCase(10003, BdStatus.Exit, null)]
        [TestCase(10004, BdStatus.Ret, null)]
        [TestCase(10005, BdStatus.Ret, null)]
        [TestCase(10006, BdStatus.Ret, null)]
        [TestCase(10007, BdStatus.Ret, null)]
        [TestCase(10008, BdStatus.Ret, null)]
        [TestCase(10009, BdStatus.Ret, null)]
        [TestCase(10010, BdStatus.Ret, null)]
        [TestCase(10011, BdStatus.Ret, null)]
        [TestCase(10012, BdStatus.Ret, null)]
        [TestCase(10013, BdStatus.Ret, null)]
        [TestCase(10014, BdStatus.Ret, null)]
        [TestCase(10015, BdStatus.Ret, "puti 12345")]
        [TestCase(10016, BdStatus.Ret, "putf 124")]
        [TestCase(10017, BdStatus.Ret, "test")]
        [TestCase(10018, BdStatus.Ret, null)]
        [TestCase(10019, BdStatus.Ret, null)]
        [TestCase(10020, BdStatus.Ret, null)]
        [TestCase(10021, BdStatus.Ret, null)]
        [TestCase(10022, BdStatus.Ret, null)]
        [TestCase(10023, BdStatus.Ret, null)]
        [TestCase(10024, BdStatus.Ret, null)]
        [TestCase(10025, BdStatus.Ret, null)]
        [TestCase(10026, BdStatus.Ret, null)]
        [TestCase(10027, BdStatus.Ret, null)]
        [TestCase(10028, BdStatus.Ret, null)]
        [TestCase(10029, BdStatus.Ret, null)]
        [TestCase(10030, BdStatus.Ret, null)]
        [TestCase(10031, BdStatus.Ret, null)]
        [TestCase(10032, BdStatus.Ret, null)]
        [TestCase(10033, BdStatus.Ret, null)]
        [TestCase(10034, BdStatus.Ret, null)]
        [TestCase(10035, BdStatus.Ret, null)]
        [TestCase(10036, BdStatus.Ret, null)]
        [TestCase(10037, BdStatus.Ret, null)]
        [TestCase(10038, BdStatus.Ret, null)]
        [TestCase(10039, BdStatus.Ret, null)]
        [TestCase(10040, BdStatus.Ret, null)]
        [TestCase(10041, BdStatus.Ret, null)]
        [TestCase(10042, BdStatus.Ret, null)]
        [TestCase(10043, BdStatus.Ret, null)]
        [TestCase(10044, BdStatus.Ret, null)]
        [TestCase(10045, BdStatus.Ret, null)]
        [TestCase(10046, BdStatus.Ret, null)]
        [TestCase(10047, BdStatus.Ret, null)]
        [TestCase(10048, BdStatus.Ret, null)]
        [TestCase(10049, BdStatus.Ret, null)]
        [TestCase(10050, BdStatus.Ret, null)]
        [TestCase(10051, BdStatus.Ret, null)]
        [TestCase(10052, BdStatus.Ret, null)]
        [TestCase(10053, BdStatus.Ret, null)]
        [TestCase(10054, BdStatus.Ret, null)]
        [TestCase(10055, BdStatus.Ret, null)]
        [TestCase(10056, BdStatus.Ret, null)]
        [TestCase(10057, BdStatus.Ret, null)]
        [TestCase(10058, BdStatus.Ret, null)]
        [TestCase(10059, BdStatus.Ret, null)]
        [TestCase(10060, BdStatus.Ret, null)]
        [TestCase(10061, BdStatus.Ret, null)]
        [TestCase(10062, BdStatus.Ret, null)]
        [TestCase(10063, BdStatus.Ret, null)]
        [TestCase(10064, BdStatus.Ret, null)]
        [TestCase(10065, BdStatus.Ret, null)]
        [TestCase(10066, BdStatus.Ret, null)]
        [TestCase(10067, BdStatus.Ret, null)]
        [TestCase(10068, BdStatus.Ret, null)]
        [TestCase(10069, BdStatus.Ret, null)]
        public void Case1(int trigger, BdStatus expectedStatus, string expectedLogOtherwiseNull) {
            var bdx = File.ReadAllBytes(Path.Combine(_scriptsDir, "case1.bdx"));
            var vm = new MobileAiExec();
            var result = vm.RunVM(bdx, trigger);
            Assert.That(result.Status, Is.EqualTo(expectedStatus));
            if (expectedLogOtherwiseNull != null) {
                Assert.That(result.Log, Is.EqualTo(expectedLogOtherwiseNull));
            }
        }
    }
}
