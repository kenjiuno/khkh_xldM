using SlimDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace khkh_xldMii.Helpers {
    public static class MatrixExtractor {
        public static Matrix[] ExtractMatricesFrom(byte[] buffer, int num) {
            var items = new Matrix[num];
            var offset = 0;
            for (int idx = 0; idx < num; idx++) {
                var m = new Matrix();
                m.M11 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M12 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M13 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M14 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M21 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M22 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M23 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M24 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M31 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M32 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M33 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M34 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M41 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M42 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M43 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M44 = BitConverter.ToSingle(buffer, offset); offset += 4;
                items[idx] = m;
            }
            return items;
        }
    }
}
