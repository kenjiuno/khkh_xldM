using khkh_xldMii.Helpers;
using khkh_xldMii.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace khkh_xldMii {
    public partial class ExportAsetOptionsForm : Form {
        private readonly Action _saveConfig;

        public ExportAsetOptionsForm(Options model, Action saveConfig) {
            _saveConfig = saveConfig;

            InitializeComponent();

            optionsBindingSource.DataSource = model;
        }

        private void ExportAsetOptionsForm_Load(object sender, EventArgs e) {

        }

        private void button2_Click(object sender, EventArgs e) {
            noesisExe.FileName = textBox1.Text;
            if (noesisExe.ShowDialog(this) == DialogResult.OK) {
                textBox1.Text = noesisExe.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            if (!ValidateChildren()) {
                return;
            }
            optionsBindingSource.EndEdit();

            _saveConfig();
        }
    }
}
