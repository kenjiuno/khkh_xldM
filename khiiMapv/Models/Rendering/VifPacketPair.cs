using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace khiiMapv.Models.Rendering {
    public class VifPacketPair {
        public byte[] VifPacket { get; }
        public int TextureIndex { get; }

        public VifPacketPair(byte[] vifPacket, int textureIndex) {
            this.VifPacket = vifPacket;
            this.TextureIndex = textureIndex;
        }
    }
}
