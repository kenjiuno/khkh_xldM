using ee1Dec;
using ee1Dec.C;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib {
    internal class MobRecUt {
        public delegate void Tx8();

        public static bool Rec2(
            uint addr,
            IDictionary<uint, MobUt.Tx8> fragments,
            CustEE ee,
            string dirSaveToDLL
        ) {
            if (dirSaveToDLL == null) {
                return false;
            }

            string flib = Myrec.Getflib(addr, dirSaveToDLL);
            if (!File.Exists(flib)) {
                Myrec.Privrec1(addr, new MemoryStream(ee.ram, false), dirSaveToDLL);
                if (!File.Exists(flib)) {
                    return false;
                }
            }

            Assembly lib = Assembly.LoadFile(flib);
            Type cls1 = lib.GetType("ee1Dec.C.Class1");
            object o = Activator.CreateInstance(cls1, ee);
            MethodInfo mi = cls1.GetMethod(LabUt.addr2Funct(addr));
            MobUt.Tx8 tx8 = (MobUt.Tx8)Delegate.CreateDelegate(typeof(MobUt.Tx8), o, mi);
            fragments[addr] = tx8;
            return true;
        }

        public static bool Rec1(uint addr, SortedDictionary<uint, MobUt.Tx8> dicti2a, CustEE ee) {
#if AllowRec1
            if (Rec2(addr, dicti2a, ee, Settings.Default.dirlib)) {
                System.Diagnostics.Debug.WriteLine("## " + addr.ToString("X8"));
                return true;
            }
            else {
                return false;
            }
#else
            return false;
#endif
        }
    }
}
