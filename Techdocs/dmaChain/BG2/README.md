# ChunkInfo

Source: [OpenKh/ModelBackground.cs at c0ab4aac41ee3fd834b08735fe241b2bc512fdfc · OpenKH/OpenKh](https://github.com/OpenKH/OpenKh/blob/c0ab4aac41ee3fd834b08735fe241b2bc512fdfc/OpenKh.Kh2/ModelBackground.cs#L34)

## Layout

<img src="map1.svg" style="background:white">

## Flags1

<img src="flags1.svg" style="background:white">

Notes:

- `SPE` = IsSpecular
- `VB` = HasVertexBuffer

## Flags2

<img src="flags2.svg" style="background:white">

Notes:

- `NoShdw` = IsShadowOff
- `A+` = IsAlphaAdd
- `A-` = IsAlphaSubtract

## Flags3

<img src="flags3.svg" style="background:white">
