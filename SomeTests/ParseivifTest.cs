using hex04BinTrack;
using khiiMapv;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SomeTests {
    public class ParseivifTest {
        [Test]
        //[TestCase(@"H:\Dev\KH2\osdanova\Chest_vif1.model", @"H:\Dev\KH2\osdanova\Chest_vif1.txt")]
        [TestCase(@"H:\Dev\KH2\osdanova\HP_Sora_VIF1.model", @"H:\Dev\KH2\osdanova\HP_Sora_VIF1.txt")]
        public void Chest_vif1(string modelFile, string textFile) {
            var text = RDForm.Parseivif.Parse(File.ReadAllBytes(modelFile));
            File.WriteAllText(textFile, text);
        }
    }
}
