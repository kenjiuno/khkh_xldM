#define UsePressedEERam
//#define AllowRec

using ee1Dec;
using ee1Dec.C;
using ef1Declib.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib {
    public partial class MobileMatrixComputer2 {
        private readonly int _offMdlxRoot;
        private readonly int _offMsetRoot;
        private readonly int _tmp1;
        private readonly int _tmp2;
        private readonly int _tmp3;
        private readonly int _tmp4;
        private readonly int _tmp5;
        private readonly int _tmp6;
        private readonly int _tmp7;
        private readonly int _tmp8;
        private readonly int _tmp9;
        private readonly int _tmpa;
        private readonly int _tmpb;
        private readonly int _tmpc;
        private readonly int _mdlxVtbl = 0x00347A28;
#if AllowRec
        private readonly string _dirSaveToDLL = @"H:\Proj\khkh_xldM\MEMO\expSim\MatrixComputer2\DLLs";
#else
        private readonly string _dirSaveToDLL = null;
#endif

        private int _offMdlx04;

        public MobileMatrixComputer2() {
            InitFragments();

            _offMdlxRoot = 20 * 1024 * 1024;
            _offMsetRoot = 25 * 1024 * 1024;
            _tmp1 = (32 * 1024 * 1024) - (768) - (65536);
            _tmp2 = (32 * 1024 * 1024) - (768) - (65536) - (65536);
            _tmp3 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536);
            _tmp4 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536);
            _tmp5 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512);
            _tmp6 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512);
            _tmp7 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512);
            _tmp8 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512) - (65536);
            _tmp9 = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512) - (65536) - (65536);
            _tmpa = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512) - (65536) - (65536) - (65536);
            _tmpb = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512) - (65536) - (65536) - (65536) - (65536);
            _tmpc = (32 * 1024 * 1024) - (768) - (65536) - (65536) - (65536) - (65536) - (16 * 512) - (16 * 512) - (16 * 512) - (65536) - (65536) - (65536) - (65536) - (65536);
        }

        private void LoadEERam() {
#if UsePressedEERam
            using (var inStream = new MemoryStream(Resources.eeram, false))
            using (var outStream = new MemoryStream(_ee.ram, true)) {
                SparseBinHelper.Decode(inStream, outStream);
            }
#else
            using (var fs = File.OpenRead(@"H:\Proj\khkh_xldM\MEMO\expSim\MatrixComputer2\MobileMatrixComputer2.bin")) {
                fs.Read(_ee.ram, 0, 32 * 1024 * 1024);
            }
#endif
        }

        public void Load(
            Stream fsMdlx,
            Stream fsMset
        ) {
            LoadEERam();

            {
                fsMdlx.Read(_ee.ram, _offMdlxRoot, 5 * 1024 * 1024);
                var relocator = new RelocMdlx(
                    _ee.ram,
                    _offMdlxRoot,
                    _offMdlxRoot,
                    (uint)_mdlxVtbl,
                    0,
                    (uint)_tmp2,
                    1
                );
                _offMdlx04 = (int)relocator.Run();
            }

            {
                var memory = new MemoryStream(_ee.ram, true);
                var writer = new BinaryWriter(memory);
                {
                    memory.Position = _tmp2;
                    var Sxyz = _tmp5;
                    var Rxyz = _tmp6;
                    var Txyz = _tmp7;
                    var array = new int[] {
                        0x0      ,0          ,0     ,0,
                        0x0      ,_offMdlx04 ,0x0   ,Sxyz,
                        Rxyz     ,Txyz       ,_tmp3 ,_tmp4,
                        0x0      ,0x0        ,0x0   ,0,
                    };
                    foreach (uint dword in array) {
                        writer.Write(dword);
                    }
                }
                {
                    memory.Position = _tmpc;
                    writer.Write(1.0f);
                    memory.Position = _tmpc + 4 * (4 + 1);
                    writer.Write(1.0f);
                    memory.Position = _tmpc + 4 * (8 + 2);
                    writer.Write(1.0f);
                    memory.Position = _tmpc + 4 * (12 + 3);
                    writer.Write(1.0f);
                }
            }

            {
                fsMset.Read(_ee.ram, _offMsetRoot, 5 * 1024 * 1024);

                var relocator = new RelocMset(
                    _ee.ram,
                    (uint)_offMsetRoot,
                    (uint)_offMsetRoot,
                    new uint[] { 0, 0, (uint)_tmp8, (uint)_tmp9, (uint)_tmpa, (uint)_tmpb, }
                );
                relocator.Run();
            }
        }

        public class ComputeResult {
            public byte[] Matrices { get; set; }
        }

        public ComputeResult Compute(int offMsetRxxx, float tick, int numFKMatrices, int numFKIKMatrices) {
            var offMset = _offMsetRoot + offMsetRxxx;

            // s0, s1, s2, s4, a1
            var s1 = offMset;
            var a0 = offMset;

            var s4 = _tmp1; // temp;
            var s2 = _tmp2; // st1;
            var a1 = s2;

            _ee.VF[0].w = 1;

            if (true) {
                _ee.at.UD0 = 0U;
                _ee.v0.UD0 = 0U;
                _ee.v1.UD0 = 0U;
                _ee.a0.UD0 = (uint)a0; // mset +0x00 // MotionPrototype0 *this
                _ee.a1.UD0 = (uint)a1; // info tbl   // ModelObj *mobj
                _ee.a2.UD0 = (uint)_tmpc;            // FMatrix *root
                _ee.a3.UD0 = 0U;                     // ToFace *toFace
                _ee.t0.UD0 = 0U;                     // IKAdjust *ikAdjust
                _ee.t1.UD0 = 0U;
                _ee.t2.UD0 = 0U;
                _ee.t3.UD0 = 0U;
                _ee.t4.UD0 = 0U;
                _ee.t5.UD0 = 0U;
                _ee.t6.UD0 = 0U;
                _ee.t7.UD0 = 0U;
                _ee.s0.UD0 = 0U;
                _ee.s1.UD0 = 0U;
                _ee.s2.UD0 = 0U;
                _ee.s3.UD0 = 0U;
                _ee.s4.UD0 = 0U;
                _ee.s5.UD0 = 0U;
                _ee.s6.UD0 = 0U;
                _ee.s7.UD0 = 0U;
                _ee.t8.UD0 = 0U;
                _ee.t9.UD0 = 0U;
                _ee.k0.UD0 = 0U;
                _ee.k1.UD0 = 0U;
                _ee.gp.UD0 = 0U;
                _ee.sp.UD0 = 0x2000000U;
                _ee.s8.UD0 = 0U;

                _ee.fpr[12].f = tick; // float time

                Exec(0x00130d28, "calc");
            }

            var buffer = new byte[0x40 * numFKIKMatrices];
            var ikBoundary = 0x40 * numFKMatrices;
            Buffer.BlockCopy(_ee.ram, _tmp4, buffer, 0, ikBoundary);
            Buffer.BlockCopy(_ee.ram, _tmpa, buffer, ikBoundary, buffer.Length - ikBoundary);

            return new ComputeResult {
                Matrices = buffer,
            };
        }

        private void Exec(uint pc, string procedureHint) {
            _ee.ra.UD0 = 0xFFFFFFFFU;
            _ee.pc = pc;
            while (_ee.pc != 0xFFFFFFFFU) {
                if (_fragments.ContainsKey(_ee.pc) || MobRecUt.Rec2(_ee.pc, _fragments, _ee, _dirSaveToDLL)) {
                    _fragments[_ee.pc]();
                }
                else {
                    throw new CodeFragmentNotFoundException(_ee.pc, procedureHint);
                }
            }
        }
    }
}
