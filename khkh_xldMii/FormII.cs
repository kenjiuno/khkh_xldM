//#define UseMultiSamp
//#define UsePerspective

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using hex04BinTrack;
using ef1Declib;
using khkh_xldMii.V;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.Collections;
using SlimDX.Direct3D9;
using SlimDX;
using khkh_xldMii.Models.Mset;
using khkh_xldMii.Utils.Mset;
using khkh_xldMii.Utils.Mdlx;
using khkh_xldMii.Models.Mdlx;
using khkh_xldMii.Models;
using khkh_xldMii.Interfaces;
using khkh_xldMii.Helpers;
using khkh_xldMii.Properties;
using System.Linq;
using static khkh_xldMii.Helpers.ExportRootMotion;
using CommandLine;
using Newtonsoft.Json;
using System.Security.Cryptography;
using LibMemoryBar;
using LibPetitMdlxTweaker;
using System.Buffers.Binary;

namespace khkh_xldMii {
    public partial class FormII : Form, ILoadf, IVwer {
        /// <summary>
        /// Player, RightHand, LeftHand
        /// </summary>
        private readonly OneModel[] _models = new OneModel[] { new OneModel(), new OneModel(), new OneModel(), };
        private readonly Options _arg;
        private readonly Action _saveConfig;
        private readonly MemoryBar _memoryBar = new MemoryBar();
        private readonly PetitMdlxTweaker _petitMdlxTweaker = new PetitMdlxTweaker();
        private float _tick = 0;
        private Device _device = null;
        private Direct3D _d3d = new Direct3D();
        private float _viewZ = 5f;
        private Vector3 _viewOffset = new Vector3(-4f, -66f, 0f);
        private Quaternion _viewRotation = new Quaternion(0f, 0f, 0f, 1f);
        private Point _mouseDownAt = Point.Empty;
        private bool _mouseDownWip = false;
        private int _reentrantLock = 0;
        private bool _captureNow = false;
        private BCForm _bcForm = null;

        public FormII(Options arg, Action saveConfig) {
            _arg = arg;
            _saveConfig = saveConfig;

            _models[1].parent = _models[0];
            _models[1].matrixIndexToAttach = 0xB2;
            _models[2].parent = _models[0];
            _models[2].matrixIndexToAttach = 0x56;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            var lvi = new ListViewItem("");
            lvi.SubItems.Add("(Drop your .mdlx file to window)");
            listView1.Items.Add(lvi);

            string[] args = Environment.GetCommandLineArgs();
            for (int x = 1; x < args.Length; x++) {
                string fmdlx = args[x];
                if (File.Exists(fmdlx) && Path.GetExtension(fmdlx).ToLower().Equals(".mdlx")) {
                    LoadMdlx(fmdlx, 0);
                    LoadMset(MatchUt.findMset(fmdlx), 0);
                }
            }

            radioButtonAny_CheckedChanged(null, null);

            //loadMdlx(@"V:\KH2.yaz0r\dump_kh2\obj\W_EX010_U0.mdlx", 1);
            //loadMset(@"H:\EMU\pcsx2-0.9.4\expa\obj_W_EX010_BTLF_R.mset", 1);
            //loadMdlx(@"V:\KH2.yaz0r\dump_kh2\obj\W_EX010_W0.mdlx", 2);
            //loadMset(@"H:\EMU\pcsx2-0.9.4\expa\obj_W_EX010_BTLF_L.mset", 2);
        }

        private class MatchUt {
            public static string findMset(string fmdlx) {
                string s1 = fmdlx.Substring(0, fmdlx.Length - 5) + ".mset";
                if (File.Exists(s1)) return s1;
                string s2 = Regex.Replace(s1, "_[a-z]+\\.", ".", RegexOptions.IgnoreCase);
                if (File.Exists(s2)) return s2;
                string s4 = Regex.Replace(s1, "_[a-z]+(_[a-z]+\\.)", "$1", RegexOptions.IgnoreCase);
                if (File.Exists(s4)) return s4;
                string s3 = Regex.Replace(s1, "_[a-z]+_[a-z]+\\.", ".", RegexOptions.IgnoreCase);
                if (File.Exists(s3)) return s3;
                return s1;
            }
        }

        private class MotionDesc {
            public OneMotion oneMotion;
            public float maxTick;
            public float minTick;
        }

        void LoadMset(string msetPath, int modelIdx) {
            var model = _models[modelIdx];
            model.DisposeMset();

            if (File.Exists(msetPath)) {
                using (FileStream fs = File.OpenRead(msetPath)) {
                    model.mset = new Msetfst(fs, Path.GetFileName(msetPath));

                    //Msetblk MB = new Msetblk(new MemoryStream(mset.al1[0].bin, false));
                    //Console.WriteLine();
                }

                if (modelIdx == 0) {
                    listView1.Items.Clear();
                    foreach (OneMotion oneMotion in model.mset.motionList) {
                        ListViewItem lvi = new ListViewItem(oneMotion.indexInMset.ToString("000"));
                        lvi.SubItems.Add(oneMotion.label);
                        listView1.Items.Add(lvi);
                        MotionDesc desc = new MotionDesc();
                        desc.oneMotion = oneMotion;
                        if (oneMotion.isRaw) {
                            AnbRawReader reader = new AnbRawReader(new MemoryStream(oneMotion.anbBin, false));
                            desc.maxTick = reader.cntFrames;
                            desc.minTick = 0;
                        }
                        else {
                            AnbReader reader = new AnbReader(new MemoryStream(oneMotion.anbBin, false));
                            desc.maxTick = (reader.Motion.FrameTimeList.Any()) ? reader.Motion.FrameTimeList[reader.Motion.FrameTimeList.Count - 1] : 0;
                            desc.minTick = (reader.Motion.FrameTimeList.Any()) ? reader.Motion.FrameTimeList[0] : 0;
                        }
                        lvi.Tag = desc;
                    }
                }
                model.binMset = File.ReadAllBytes(msetPath);
            }
            model.emuRunner = null;
        }

        private class TexturePair {
            public byte texIndex;
            public byte faceIndex;

            public TexturePair(byte texIndex, byte faceIndex) {
                this.texIndex = texIndex;
                this.faceIndex = faceIndex;
            }
        }

        private class OneModel : IDisposable {
            public Mdlxfst mdlx = null;
            public Texex2[] timc = null;
            public Texex2 timf = null;
            public Msetfst mset = null;
            public List<Texture> altex = new List<Texture>();
            public List<Texture> altex1 = new List<Texture>();
            public List<Texture> altex2 = new List<Texture>();
            public List<Body1> albody1 = new List<Body1>();
            public byte[] binMdlx;
            public byte[] binMset;
            public TrianglesContainer triangles = new TrianglesContainer();
            public MobileMatrixComputer2 emuRunner = null;
            public TexturePair[] texturePairList = new TexturePair[0];

            public Matrix[] matrixList = null; // for keyblade
            public Matrix[] fkIkMatrixList = null;
            public OneModel parent = null; // for keyblade
            public int matrixIndexToAttach = -1; // for keyblade
            public ExportRootMotion exportRootMotion;

            public bool Present { get { return mdlx != null && mset != null; } }

            #region IDisposable メンバ

            public void Dispose() {
                DisposeMdlx();
                DisposeMset();
            }

            #endregion

            public void DisposeMset() {
                mset = null;
                binMset = null;
                emuRunner = null;
            }

            public void DisposeMdlx() {
                mdlx = null;
                timc = null;
                timf = null;
                altex.Clear();
                altex1.Clear();
                altex2.Clear();
                albody1.Clear();
                binMdlx = null;
                triangles.Close();
                emuRunner = null;
                matrixList = null;
            }
        }

        private void LoadMdlx(string fmdlx, int modelIdx) {
            if (modelIdx == 0) {
                listView1.Items.Clear();
                var lvi = new ListViewItem("");
                lvi.SubItems.Add("(Drop your .mdlx file to window)");
                listView1.Items.Add(lvi);
            }

            var model = _models[modelIdx];
            model.DisposeMdlx();

            if (modelIdx == 0) {
                _models[1].Dispose();
                _models[2].Dispose();
            }

            ReadBar.Barent[] ents;
            using (FileStream fs = File.OpenRead(fmdlx)) {
                ents = ReadBar.Explode(fs);
                foreach (ReadBar.Barent ent in ents) {
                    switch (ent.k) {
                        case 7:
                            model.timc = TIMCollection.Load(new MemoryStream(ent.bin, false));
                            model.timf = (model.timc.Length >= 1) ? model.timc[0] : null;
                            break;
                        case 4:
                            model.mdlx = new Mdlxfst(new MemoryStream(ent.bin, false));
                            break;
                    }
                }
            }
            model.binMdlx = File.ReadAllBytes(fmdlx);
            model.emuRunner = null;
            ReloadTexture(modelIdx);
            CalcBody(model.triangles, model, null, 0, UpdateFlags.Base);
        }

        private void FormII_DragEnter(object sender, DragEventArgs e) {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop) ? DragDropEffects.Copy : DragDropEffects.None;
        }

        private void FormII_DragDrop(object sender, DragEventArgs e) {
            var filePathList = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (filePathList != null) {
                foreach (string filePath in filePathList) {
                    if (Path.GetExtension(filePath).ToLower().Equals(".mdlx")) {
                        var toLoad = filePath;
                        Defer(
                            () => {
                                LoadMdlx(toLoad, 0);
                                LoadMset(MatchUt.findMset(toLoad), 0);
                                Recalc();
                            }
                        );
                        break;
                    }
                }
            }
        }

        private void Defer(Action action) {
            var timer = new Timer();
            timer.Interval = 100;
            timer.Tick += new EventHandler(
                (a, b) => {
                    timer.Dispose();
                    action();
                }
            );
            timer.Start();
        }

        private class TrianglesContainer : IDisposable {
            public VertexBuffer vertexBuffer { get; set; }
            public VertexFormat vertexFormat { get; set; }
            public int CountPrimitives { get; set; }
            public int CountVertices { get; set; }
            public RenderPartition[] Partitions { get; set; }
            public uint[] MixedIndexList { get; set; }

            #region IDisposable メンバ

            public void Dispose() {
                Close();
            }

            #endregion

            public void Close() {
                vertexBuffer?.Dispose();
                vertexBuffer = null;
                vertexFormat = VertexFormat.None;
                CountPrimitives = 0;
                CountVertices = 0;
            }
        }

        private class RenderPartition {
            public int StartVertexIndex { get; set; }
            public int CountPrimitives { get; set; }
            public int TextureIndex { get; set; }
            public int Index { get; set; }

            public RenderPartition(int startVertexIndex, int cntPrimitives, int texIdx, int index) {
                this.StartVertexIndex = startVertexIndex;
                this.CountPrimitives = cntPrimitives;
                this.TextureIndex = texIdx;
                this.Index = index;
            }
        }

        void Recalc() {
            //foreach (Mesh M in _Sora) M.ctb.Close();

            foreach (ListViewItem lvi in listView1.SelectedItems) {
                if (lvi.Tag != null) {
                    MotionSelected(lvi.Tag as MotionDesc);
                }
                break;
            }
            panel1.Invalidate();
        }

        private void MotionSelected(MotionDesc tag) {
            CalcBody(_models[0].triangles, _models[0], tag.oneMotion, _tick, UpdateFlags.Animate);

            if (_models[1].Present) {
                var weapon = UtwexMotionSel.Sel(tag.oneMotion.indexInMset, _models[1].mset.motionList);
                if (weapon != null && _models[1].matrixIndexToAttach != -1) {
                    if (_models[1].Present) {
                        CalcBody(_models[1].triangles, _models[1], weapon, _tick, UpdateFlags.Animate);
                    }
                }
            }
            if (_models[2].Present) {
                var weapon = UtwexMotionSel.Sel(tag.oneMotion.indexInMset, _models[2].mset.motionList);
                if (weapon != null && _models[2].matrixIndexToAttach != -1) {
                    if (_models[2].Present) {
                        CalcBody(_models[2].triangles, _models[2], weapon, _tick, UpdateFlags.Animate);
                    }
                }
            }

            ProcessAboutTexAnim(_models[0], (float)numericUpDownTick.Value, tag);
        }

        private void ProcessAboutTexAnim(OneModel model, float tick, MotionDesc tag) {
            model.texturePairList = SelTexfacUt.Sel(model.timf.facePatchList, tick, tag.oneMotion.faceModSet);
        }

        private class UtwexMotionSel {
            public static OneMotion Sel(int index, List<OneMotion> al1) {
                foreach (OneMotion o in al1) {
                    if (o.indexInMset == index) {
                        return o;
                    }
                }
                return null;
            }
        }

        [Flags]
        private enum UpdateFlags : uint {
            None = 0x00,
            Body = 0x01,
            Transforms = 0x02,
            Motion = 0x04,
            Indices = 0x08,
            Vertices = 0x10,

            Buffers = Indices | Vertices,
            Base = Body | Transforms | Motion | Buffers,
            Animate = Body | Transforms | Motion | Vertices,
        }

        private void CalcBody(
            TrianglesContainer triangles,
            OneModel model,
            OneMotion motion,
            float tick,
            UpdateFlags flags
        ) {
            Mdlxfst mdlx = model.mdlx;
            Msetfst mset = model.mset;
            List<Body1> bodyList = model.albody1;
            var emuRunner = model.emuRunner;

            if ((flags & UpdateFlags.Body) != UpdateFlags.None) {
                bodyList.Clear();
            }

            if (mdlx != null) {
                K2Model t31 = mdlx.alt31[0];

                var basePose = t31.basePose;
                var inversedPose = t31.inversedPose;

                if (true
                    && motion != null
                    && ((flags & UpdateFlags.Motion) != UpdateFlags.None)
                ) {
                    if (motion.isRaw) {
                        AnbRawReader blk = new AnbRawReader(new MemoryStream(motion.anbBin, false));
                        int t0 = Math.Max(0, Math.Min(blk.cntFrames - 1, (int)Math.Floor(tick)));
                        int t1 = Math.Max(0, Math.Min(blk.cntFrames - 1, (int)Math.Ceiling(tick)));
                        if (t0 == t1) {
                            AnbRawAnimFrame rm = blk.frameSet[t0];
                            basePose = model.matrixList = rm.matrixList.ToArray();
                        }
                        else {
                            AnbRawAnimFrame frame1 = blk.frameSet[t0]; float f1 = tick % 1.0f;
                            AnbRawAnimFrame frame2 = blk.frameSet[t1]; float f0 = 1.0f - f1;
                            basePose = model.matrixList = new Matrix[blk.cntJoints];
                            for (int t = 0; t < basePose.Length; t++) {
                                var m = new Matrix();
                                m.M11 = frame1.matrixList[t].M11 * f0 + frame2.matrixList[t].M11 * f1;
                                m.M21 = frame1.matrixList[t].M21 * f0 + frame2.matrixList[t].M21 * f1;
                                m.M31 = frame1.matrixList[t].M31 * f0 + frame2.matrixList[t].M31 * f1;
                                m.M41 = frame1.matrixList[t].M41 * f0 + frame2.matrixList[t].M41 * f1;
                                m.M12 = frame1.matrixList[t].M12 * f0 + frame2.matrixList[t].M12 * f1;
                                m.M22 = frame1.matrixList[t].M22 * f0 + frame2.matrixList[t].M22 * f1;
                                m.M32 = frame1.matrixList[t].M32 * f0 + frame2.matrixList[t].M32 * f1;
                                m.M42 = frame1.matrixList[t].M42 * f0 + frame2.matrixList[t].M42 * f1;
                                m.M13 = frame1.matrixList[t].M13 * f0 + frame2.matrixList[t].M13 * f1;
                                m.M23 = frame1.matrixList[t].M23 * f0 + frame2.matrixList[t].M23 * f1;
                                m.M33 = frame1.matrixList[t].M33 * f0 + frame2.matrixList[t].M33 * f1;
                                m.M43 = frame1.matrixList[t].M43 * f0 + frame2.matrixList[t].M43 * f1;
                                m.M14 = frame1.matrixList[t].M14 * f0 + frame2.matrixList[t].M14 * f1;
                                m.M24 = frame1.matrixList[t].M24 * f0 + frame2.matrixList[t].M24 * f1;
                                m.M34 = frame1.matrixList[t].M34 * f0 + frame2.matrixList[t].M34 * f1;
                                m.M44 = frame1.matrixList[t].M44 * f0 + frame2.matrixList[t].M44 * f1;
                                basePose[t] = m;
                            }
                        }
                    }
                    else {
                        var anb = new AnbReader(new MemoryStream(motion.anbBin, false));
                        if (emuRunner == null) {
                            emuRunner = model.emuRunner = new MobileMatrixComputer2();
                            emuRunner.Load(
                                new MemoryStream(model.binMdlx, false),
                                new MemoryStream(model.binMset, false)
                            );
                        }
                        var result = emuRunner.Compute((int)motion.anbOff, tick, anb.NumFKBones, anb.NumFKIKBones);

                        basePose = model.matrixList = MatrixExtractor.ExtractMatricesFrom(result.Matrices, anb.NumFKBones);
                        model.fkIkMatrixList = MatrixExtractor.ExtractMatricesFrom(result.Matrices, anb.NumFKIKBones);

                        var numFk = anb.NumFKBones;
                        var fkBones = model.mdlx.ModelList
                            .FirstOrDefault()?
                            .boneList?
                            .bones;
                        var rootPos = anb.Motion.RootPosition;
                        var fcurves = anb.Motion.FCurvesFKList
                            .Concat(anb.Motion.FCurvesIKList)
                            .ToArray();
                        model.exportRootMotion = new ExportRootMotion {
                            NumFk = numFk,
                            NumIk = anb.NumFKIKBones - anb.NumFKBones,
                            RootPosition = new RootPositionClass {
                                ScalingX = rootPos.ScalingX,
                                ScalingY = rootPos.ScalingY,
                                ScalingZ = rootPos.ScalingZ,
                                RotationX = rootPos.RotationX,
                                RotationY = rootPos.RotationY,
                                RotationZ = rootPos.RotationZ,
                                TranslationX = rootPos.TranslationX,
                                TranslationY = rootPos.TranslationY,
                                TranslationZ = rootPos.TranslationZ,
                                FCurves = rootPos.FCurves,
                                FCurvesTargetBoneIndexList = rootPos.FCurves
                                    .Select(fCurveIndex => (fCurveIndex < 0) ? -1 : numFk + fcurves[fCurveIndex].TargetBoneIndex)
                                    .ToArray(),
                            },
                            FullBones = new BoneClass[0]
                                .Concat(
                                    fkBones?
                                        .Select(
                                            bone => new BoneClass {
                                                Index = bone.cur,
                                                Parent = bone.parent,
                                            }
                                        ) ?? new BoneClass[0]
                                )
                                .Concat(
                                    anb.Motion.IKBones
                                        .Select(
                                            bone => new BoneClass {
                                                Index = bone.cur,
                                                Parent = bone.parent,
                                            }
                                        )
                                )
                                .ToArray(),
                        };
                    }
                }

                if ((flags & UpdateFlags.Transforms) != UpdateFlags.None) {
                    int cnt = basePose.Length;
                    int mn;
                    for (mn = 0; mn < cnt; ++mn) {
                        inversedPose[mn] = Matrix.Invert(basePose[mn]);
                    }
                }

                var viewMatrix = Matrix.Identity;
                if (model.parent != null && model.parent.matrixList != null && model.matrixIndexToAttach != -1) {
                    viewMatrix = model.parent.matrixList[model.matrixIndexToAttach];
                }

                if ((flags & UpdateFlags.Body) != UpdateFlags.None) {
                    foreach (K2Vif t13 in t31.vifList) {
                        int tops = 0x220;
                        int top2 = 0;
                        var vu1mem = new VU1Mem();
                        new ParseVIF1(vu1mem).Parse(new MemoryStream(t13.bin, false), tops);
                        var body1 = SimaVU1.Sima(vu1mem, basePose, tops, top2, t13.textureIndex, t13.marixIndexArray, viewMatrix);
                        bodyList.Add(body1);
                    }
                }

                if ((flags & UpdateFlags.Indices) != UpdateFlags.None) {
                    if (true) {
                        var mixedIndexList = new List<uint>();
                        var partitions = new List<RenderPartition>();
                        if (true) {
                            int startVtxIdx = 0;
                            int partitionIndex = 0;
                            uint[] ringVtxIdx = new uint[4];
                            int ringIdx = 0;
                            int tickInt = (int)tick;
                            int[] ord = new int[] { 1, 2, 3 };
                            foreach (Body1 body in bodyList) {
                                int numTriangles = 0;
                                for (int stripIdx = 0; stripIdx < body.alvi.Length; stripIdx++) {
                                    ringVtxIdx[ringIdx] = (uint)((body.alvi[stripIdx]) | (partitionIndex << 12) | (stripIdx << 24));
                                    ringIdx = (ringIdx + 1) & 3;
                                    if (body.alfl[stripIdx] == 0x20) {
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(0 + (tickInt * 103)) % 3]) & 3]);
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(1 + (tickInt * 103)) % 3]) & 3]);
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(2 + (tickInt * 103)) % 3]) & 3]);
                                        numTriangles++;
                                    }
                                    else if (body.alfl[stripIdx] == 0x30) {
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(0 + (tickInt << 1)) % 3]) & 3]);
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(2 + (tickInt << 1)) % 3]) & 3]);
                                        mixedIndexList.Add(ringVtxIdx[(ringIdx - ord[(1 + (tickInt << 1)) % 3]) & 3]);
                                        numTriangles++;
                                    }
                                }
                                partitions.Add(new RenderPartition(startVtxIdx, numTriangles, body.t, partitionIndex));
                                startVtxIdx += 3 * numTriangles;
                                partitionIndex++;
                            }
                        }

                        triangles.Partitions = partitions.ToArray();
                        triangles.MixedIndexList = mixedIndexList.ToArray();
                    }
                }

                if ((flags & UpdateFlags.Vertices) != UpdateFlags.None) {
                    triangles.CountVertices = triangles.MixedIndexList?.Length ?? 0;
                    triangles.CountPrimitives = 0;

                    if (triangles.CountVertices != 0) {
                        triangles.vertexBuffer?.Dispose();
                        triangles.vertexBuffer = new VertexBuffer(
                            _device,
                            PTex3.Size * triangles.CountVertices,
                            0,
                            triangles.vertexFormat = PTex3.Format,
                            Pool.Managed
                            );
                        DataStream gs = triangles.vertexBuffer.Lock(0, 0, 0);
                        try {
                            int cx = triangles.MixedIndexList.Length;
                            for (int x = 0; x < cx; x++) {
                                uint mixed = triangles.MixedIndexList[x];
                                uint index = mixed & 4095;
                                uint partitionIndex = (mixed >> 12) & 4095;
                                uint stripIndex = (mixed >> 24) & 4095;
                                var body = bodyList[(int)partitionIndex];
                                var vertex = new PTex3(body.alvert[index], new Vector2(body.aluv[stripIndex].X, body.aluv[stripIndex].Y));
                                gs.Write(vertex);
                            }
                            gs.Position = 0;
                        }
                        finally {
                            triangles.vertexBuffer.Unlock();
                        }
                    }
                }
            }
        }

        private PresentParameters DefaultPresentParameters {
            get {
                PresentParameters pp = new PresentParameters();
                pp.Windowed = true;
                pp.SwapEffect = SwapEffect.Discard;
                pp.EnableAutoDepthStencil = true;
                pp.AutoDepthStencilFormat = Format.D24X8;
                pp.BackBufferWidth = panel1.ClientSize.Width;
                pp.BackBufferHeight = panel1.ClientSize.Height;
                return pp;
            }
        }

        private void panel1_Load(object sender, EventArgs e) {
            _device = new Device(_d3d, 0, DeviceType.Hardware, panel1.Handle, CreateFlags.SoftwareVertexProcessing, DefaultPresentParameters);
            ResetRenderState();

            Resized();

            panel1.MouseWheel += new MouseEventHandler(panel1_MouseWheel);
        }

        void panel1_MouseWheel(object sender, MouseEventArgs e) {
            _viewZ = Math.Max(1.0f, Math.Min(100.0f, _viewZ + (e.Delta / 120)));
            ResizedAndInvalid();
        }

        private void ResizedAndInvalid() {
            Resized();
            panel1.Invalidate();
        }

        private void Resized() {
            int cxw = panel1.Width;
            int cyw = panel1.Height;
            float fx = (cxw > cyw) ? ((float)cxw / cyw) : 1.0f;
            float fy = (cxw < cyw) ? ((float)cyw / cxw) : 1.0f;

            float fact = (float)(_viewZ * 0.5f * 100);

            if (checkBoxPerspective.Checked) {
                float fnear = Convert.ToSingle(numnear.Value);
                float ffar = Convert.ToSingle(numfar.Value);
                float fox = Convert.ToSingle(numx.Value);
                float foy = Convert.ToSingle(numy.Value);
                float foz = Convert.ToSingle(numz.Value);
                float ffov = Convert.ToSingle(numfov.Value) / 180.0f * 3.14159f;

                _device.SetTransform(TransformState.Projection, Matrix.PerspectiveFovRH(
                    ffov, (cxw != 0) ? ((float)cxw / cyw) : 1, fnear, ffar
                    ));
                var viewMatrix = Matrix.RotationQuaternion(_viewRotation);
                viewMatrix.M41 += _viewOffset.X + fox;
                viewMatrix.M42 += _viewOffset.Y + foy;
                viewMatrix.M43 += _viewOffset.Z + foz;
                _device.SetTransform(TransformState.View, viewMatrix);
            }
            else {
                _device.SetTransform(TransformState.Projection, Matrix.OrthoLH(
                    fx * fact, fy * fact, fact * 10, fact * -10
                    ));
                var viewMatrix = Matrix.RotationQuaternion(_viewRotation);
                viewMatrix.M41 += _viewOffset.X;
                viewMatrix.M42 += _viewOffset.Y;
                viewMatrix.M43 += _viewOffset.Z;
                _device.SetTransform(TransformState.View, viewMatrix);
            }
        }

        private void ResetRenderState() {
            _device.SetRenderState(RenderState.Lighting, false);
            _device.SetRenderState(RenderState.ZEnable, true);

            _device.SetRenderState(RenderState.AlphaBlendEnable, true);
            _device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            _device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);

            //device.SetRenderState(RenderState.CullMode, Cull.Clockwise);

            ReloadTexture(-1);
        }

        private class SelTexfacUt {
            public static TexturePair[] Sel(List<FacePatch> alp, float tick, FaceModSet fm) {
                TexturePair[] sel = new TexturePair[alp.Count];
                foreach (FaceMod f1 in fm.list) {
                    if (f1.v2 != -1 && f1.v0 <= tick && tick < f1.v2) {
                        for (int x = 0; x < alp.Count; x++) {
                            int curt = (int)(tick - f1.v0) / 8;
                            foreach (FaceTexture tf in alp[x].faceTextureList) {
                                if (tf.i0 == f1.v6) {
                                    if (curt <= 0) {
                                        if (sel[x] == null) {
                                            sel[x] = new TexturePair((byte)alp[x].textureIndex, (byte)tf.v6);
                                            break;
                                        }
                                    }
                                    curt -= tf.v2;
                                }
                            }
                        }
                    }
                }
                return sel;
            }
        }

        private class TUt {
            public static Texture FromBitmap(Device device, Bitmap p) {
                MemoryStream os = new MemoryStream();
                p.Save(os, ImageFormat.Png);
                os.Position = 0;
                return Texture.FromStream(device, os);
            }
        }

        private void ReloadTexture(int modelIdx) {
            if (_device != null) {
                int x = 0;
                foreach (OneModel model in _models) {
                    if (x == modelIdx || modelIdx == -1) {
                        model.altex.Clear();
                        model.altex1.Clear();
                        model.altex2.Clear();
                        if (model.timf != null) {
                            //int t = 0;
                            foreach (TIMBitmap st in model.timf.bitmapList) {
                                model.altex.Add(TUt.FromBitmap(_device, st.bitmap));
                                //st.pic.Save(@"H:\Proj\khkh_xldM\MEMO\pattex\t" + ty + "." + t + ".png", ImageFormat.Png); t++;
                            }
                            if (x == 0) {
                                for (int p = 0; p < 2; p++) {
                                    for (int pi = 0; ; pi++) {
                                        Bitmap pic = model.timf.GetPatchBitmap(p, pi);
                                        if (pic == null)
                                            break;
                                        //pic.Save(@"H:\Proj\khkh_xldM\MEMO\pattex\p" + p + "." + pi + ".png", ImageFormat.Png);
                                        switch (p) {
                                            case 0: model.altex1.Add(TUt.FromBitmap(_device, pic)); break;
                                            case 1: model.altex2.Add(TUt.FromBitmap(_device, pic)); break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    x++;
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e) {
            if (_device == null || _device.TestCooperativeLevel().IsFailure) return;

            bool isWire = false;// radioButtonWire.Checked;
            bool isTexsk = true;// radioButtonTex.Checked;
            bool isLooksChange = checkBoxLooks.Checked;

            _device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, panel1.BackColor, 1.0f, 0);
            _device.BeginScene();
            _device.SetRenderState(RenderState.FillMode, isWire ? FillMode.Wireframe : FillMode.Solid);
            foreach (OneModel model in _models) {
                var triangles = model.triangles;
                if (triangles != null && triangles.vertexBuffer != null) {
                    _device.SetStreamSource(0, triangles.vertexBuffer, 0, PTex3.Size);
                    _device.VertexFormat = triangles.vertexFormat;
                    _device.Indices = null;
                    foreach (RenderPartition partition in triangles.Partitions) {
                        if (true) {
                            _device.SetTextureStageState(1, TextureStage.ColorOperation, TextureOperation.Disable);
                            _device.SetTextureStageState(2, TextureStage.ColorOperation, TextureOperation.Disable);
                            _device.SetTexture(0, null);

                            var textureList = new List<BaseTexture>();

                            if (isTexsk) {
                                if (partition.TextureIndex < model.altex.Count) {
                                    textureList.Add(model.altex[partition.TextureIndex]);
                                }
                                if (isLooksChange && model.texturePairList.Length >= 1 && model.texturePairList[0] != null && partition.TextureIndex == model.texturePairList[0].texIndex) {
                                    textureList.Add(model.altex1[model.texturePairList[0].faceIndex]);
                                }
                                if (isLooksChange && model.texturePairList.Length >= 2 && model.texturePairList[1] != null && partition.TextureIndex == model.texturePairList[1].texIndex) {
                                    textureList.Add(model.altex2[model.texturePairList[1].faceIndex]);
                                }

                                textureList.Remove(null);

                                _device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.SelectArg1);
                                _device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
                                _device.SetTexture(0, (textureList.Count < 1) ? null : textureList[0]);

                                if (textureList.Count >= 2) {
                                    _device.SetTextureStageState(1, TextureStage.ColorOperation, TextureOperation.BlendTextureAlpha);
                                    _device.SetTextureStageState(1, TextureStage.ColorArg1, TextureArgument.Texture);
                                    _device.SetTextureStageState(1, TextureStage.ColorArg2, TextureArgument.Current);
                                    _device.SetTexture(1, textureList[1]);

                                    if (textureList.Count >= 3) {
                                        _device.SetTextureStageState(2, TextureStage.ColorOperation, TextureOperation.BlendTextureAlpha);
                                        _device.SetTextureStageState(2, TextureStage.ColorArg1, TextureArgument.Texture);
                                        _device.SetTextureStageState(2, TextureStage.ColorArg2, TextureArgument.Current);
                                        _device.SetTexture(2, textureList[2]);
                                    }
                                }
                            }

                            _device.DrawPrimitives(PrimitiveType.TriangleList, partition.StartVertexIndex, partition.CountPrimitives);
                        }
                    }
                }
            }
            _device.EndScene();

            if (_captureNow) {
                _captureNow = false;
                using (Surface bb = _device.GetBackBuffer(0, 0)) {
                    int frame = (int)numericUpDownFrame.Value;

                    if (checkBoxAsPNG.Checked) {
                        Surface.ToFile(bb, "_cap" + frame.ToString("00000") + ".png", ImageFileFormat.Png);
                    }
                    else {
                        Surface.ToFile(bb, "_cap" + frame.ToString("00000") + ".jpg", ImageFileFormat.Jpg);
                    }

                    numericUpDownFrame.Value += 1;
                }
            }

            _device.Present();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e) {
            foreach (int sel in listView1.SelectedIndices) {
                numericUpDownTick.Value = 0;
                checkBoxAutoFill_CheckedChanged(null, null);
                Recalc();
                break;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e) {
            if (0 != (e.Button & MouseButtons.Left)) {
                _mouseDownAt = e.Location;
                _mouseDownWip = true;
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e) {
            if (0 != (e.Button & MouseButtons.Left) && _mouseDownWip) {
                _mouseDownWip = false;
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e) {
            if (0 != (e.Button & MouseButtons.Left) && _mouseDownWip) {
                int dx = e.X - _mouseDownAt.X;
                int dy = e.Y - _mouseDownAt.Y;
                if (dx != 0 || dy != 0) {
                    _mouseDownAt = e.Location;

                    if (0 == (ModifierKeys & Keys.Shift)) {
                        _viewRotation *= Quaternion.RotationYawPitchRoll(dx / 100.0f, dy / 100.0f, 0);
                    }
                    else {
                        _viewOffset += (new Vector3(dx, -dy, 0));
                    }
                    ResizedAndInvalid();
                }
            }
        }

        private void checkBoxAnim_CheckedChanged(object sender, EventArgs e) {
            timerTick.Enabled = checkBoxAnim.Checked;
        }

        private void timerTick_Tick(object sender, EventArgs e) {
            try {
                if (_reentrantLock++ == 0) {
                    try {
                        _tick = (float)(numericUpDownTick.Value + numericUpDownStep.Value);

                        if (checkBoxAutoNext.Checked && (float)numericUpDownAutoNext.Value <= _tick) {
                            if (checkBoxKeepCur.Checked) {
                                _tick = 1;
                            }
                            else {
                                foreach (int s in listView1.SelectedIndices) {
                                    int sel = s + 1;
                                    if (sel < listView1.Items.Count) {
                                        listView1.Items[s].Selected = false;
                                        listView1.Items[sel].Selected = true;

                                        checkBoxAutoFill_CheckedChanged(null, null);
                                    }
                                    else {
                                        checkBoxAnim.Checked = false;
                                    }
                                    break;
                                }
                            }
                        }

                        if (checkBoxAutoRec.Checked) {
                            _captureNow = true;
                        }

                        numericUpDownTick.Value = (decimal)_tick;
                    }
                    catch (Exception) {
                        timerTick.Stop();
                        checkBoxAnim.Checked = false;
                        throw;
                    }
                }
            }
            finally {
                _reentrantLock--;
            }
        }

        private void numericUpDownTick_ValueChanged(object sender, EventArgs e) {
            _tick = (float)numericUpDownTick.Value;
            Recalc();
        }

        private void checkBoxAutoFill_CheckedChanged(object sender, EventArgs e) {
            if (checkBoxAutoFill.Checked) {
                foreach (ListViewItem lvi in listView1.SelectedItems) {
                    numericUpDownAutoNext.Value = (decimal)((MotionDesc)lvi.Tag).maxTick + numericUpDownStep.Value;
                    break;
                }
            }
        }

        private void panel1_KeyDown(object sender, KeyEventArgs e) {
            switch (e.KeyCode) {
                case Keys.End:
                    Debug.WriteLine("_viewZ = " + _viewZ + "f;");
                    Debug.WriteLine("_viewOffset = new Vector3(" + _viewOffset.X + "f, " + _viewOffset.Y + "f, " + _viewOffset.Z + "f);");
                    Debug.WriteLine("_viewRotation = new Quaternion(" + _viewRotation.X + "f, " + _viewRotation.Y + "f, " + _viewRotation.Z + "f, " + _viewRotation.W + "f);");
                    break;
                case Keys.Home:
                    _viewZ = 1;
                    _viewOffset = Vector3.Zero;
                    _viewRotation = Quaternion.Identity;
                    ResizedAndInvalid();
                    break;
                case Keys.PageDown:
                    _viewZ = 4f;
                    _viewOffset = new Vector3(28f, -92f, 0f);
                    _viewRotation = new Quaternion(-0.01664254f, -0.1349622f, -0.01049327f, 0.9906555f);
                    ResizedAndInvalid();
                    break;
                case Keys.PageUp:
                    _viewZ = 5f;
                    _viewOffset = new Vector3(-4f, -66f, 0f);
                    _viewRotation = new Quaternion(0f, 0f, 0f, 1f);
                    ResizedAndInvalid();
                    break;
            }
        }

        private void checkBoxKeys_CheckedChanged(object sender, EventArgs e) {
            labelHelpKeys.Visible = checkBoxKeys.Checked;
        }

        private void fl1_DragEnter(object sender, DragEventArgs e) {
            e.Effect = (e.Data != null && e.Data.GetDataPresent(DataFormats.FileDrop)) ? DragDropEffects.Copy : e.Effect;
        }

        private void fl1_DragDrop(object sender, DragEventArgs e) {
            MessageBox.Show("ok");
        }

        private void buttonBC_Click(object sender, EventArgs e) {
            if (_bcForm == null || (_bcForm != null && _bcForm.IsDisposed)) {
                _bcForm = new BCForm(this);
            }
            _bcForm.Show();
            _bcForm.Activate();
        }

        #region ILoadf メンバ

        public void LoadOf(int x, string fp) {
            using (WC wc = new WC()) {
                switch (x) {
                    case 0: LoadMdlx(fp, 0); break;
                    case 1: LoadMset(fp, 0); break;

                    case 2: LoadMdlx(fp, 1); break;
                    case 3: LoadMset(fp, 1); break;

                    case 4: LoadMdlx(fp, 2); break;
                    case 5: LoadMset(fp, 2); break;

                    default: throw new NotSupportedException();
                }
            }
        }

        public void SetJointOf(int x, int joint) {
            switch (x) {
                case 1: _models[1].matrixIndexToAttach = joint; break;
                case 2: _models[2].matrixIndexToAttach = joint; break;

                default: throw new NotSupportedException();
            }
        }

        public void DoRecalc() {
            Recalc();
            panel1.Invalidate();
        }

        #endregion

        #region IVwer メンバ

        public void BackToViewer() {
            Activate();
        }

        #endregion

        private void checkBoxLooks_CheckedChanged(object sender, EventArgs e) {
            panel1.Invalidate();
        }

        private void radioButtonAny_CheckedChanged(object sender, EventArgs e) {
            if (false) { }
            else if (radioButton10fps.Checked) {
                timerTick.Interval = 1000 / 10;
            }
            else if (radioButton30fps.Checked) {
                timerTick.Interval = 1000 / 30;
            }
            else if (radioButton60fps.Checked) {
                timerTick.Interval = 1000 / 60;
            }
        }

        private void panel1_Resize(object sender, EventArgs e) {
            if (_device != null) {
                _device.Reset(DefaultPresentParameters);
                panel1.Invalidate();
                Resized();
            }
        }

        private void numnear_ValueChanged(object sender, EventArgs e) {
            panel1.Invalidate();
            Resized();
        }

        private void checkBoxPerspective_CheckedChanged(object sender, EventArgs e) {
            panel1.Invalidate();
            Resized();
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e) {
            listView1.ListViewItemSorter = new SortByText(e.Column);
            listView1.Sort();
        }

        private class SortByText : IComparer {
            private readonly int _column;

            public SortByText(int column) {
                _column = column;
            }

            public int Compare(object x, object y) {
                var a = (ListViewItem)x;
                var b = (ListViewItem)y;

                var s = a.SubItems[_column].Text;
                var t = b.SubItems[_column].Text;

                return s.CompareTo(t);
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            ExportStatus status = (e.UserState as ExportStatus);

            // update animation status
            progressForm.animProgressBar.Minimum = 0;
            progressForm.animProgressBar.Maximum = status.animCount;
            progressForm.animProgressBar.Value = status.animIndex;
            // update frame status
            progressForm.frameProgressBar.Minimum = 0;
            progressForm.frameProgressBar.Maximum = status.frameCount;
            progressForm.frameProgressBar.Value = status.frameIndex;

            progressForm.animProgressLabel.Text = string.Format("[{0,5} /{1,5} ] - {2}", status.animIndex, status.animCount, status.animName);
            progressForm.frameProgressLabel.Text = string.Format("[{0,5} /{1,5} ]", status.frameIndex, status.frameCount);

            switch ((ExportState)e.ProgressPercentage) {
                case ExportState.Initialization: {
                        progressForm.statusLabel.Text = "Initializing...";
                    }
                    break;
                case ExportState.Processing: {
                        progressForm.statusLabel.Text = "Processing...";
                    }
                    break;
                case ExportState.Saving: {
                        progressForm.statusLabel.Text = "Saving...";
                    }
                    break;
                case ExportState.Finished: {
                        progressForm.statusLabel.Text = "Finished!";
                    }
                    break;
                case ExportState.Cancelling: {
                        progressForm.statusLabel.Text = "Export Aborted.";
                    }
                    break;
            }
        }

        private void worker_ProgressCompleted(object sender, RunWorkerCompletedEventArgs e) {
            progressForm.Close();
            if (!e.Cancelled)
                MessageBox.Show("Animation transforms dumped.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void worker_CancelProgress(object sender, EventArgs e) {
            backgroundWorker1.CancelAsync();
        }

        private void buttonExportAset_Click(object sender, EventArgs e) {
            progressForm = new ExportProgress();

            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;

            backgroundWorker1.DoWork += ExportASET;
            backgroundWorker1.ProgressChanged += worker_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted += worker_ProgressCompleted;

            progressForm.cancelExportButton.Click += worker_CancelProgress;

            ListViewItem[] items = null;

            if (listView1.CheckedItems.Count > 0) {
                items = new ListViewItem[listView1.CheckedItems.Count];
                listView1.CheckedItems.CopyTo(items, 0);
            }
            else if (listView1.Items.Count > 0) {
                items = new ListViewItem[listView1.Items.Count];
                listView1.Items.CopyTo(items, 0);
            }

            if (items != null) {
                backgroundWorker1.RunWorkerAsync(
                    items
                        .Select(
                            listViewItem => new ExportThisOne {
                                Text = listViewItem.SubItems
                                    .Cast<ListViewItem.ListViewSubItem>()
                                    .Skip(1)
                                    .FirstOrDefault()?
                                    .Text ?? listViewItem.Text,
                                MotionDesc = (MotionDesc)listViewItem.Tag,
                            }
                        )
                        .ToArray()
                );

                progressForm.ShowDialog(this);
            }

            backgroundWorker1.CancelAsync();
            backgroundWorker1.DoWork -= ExportASET;
            backgroundWorker1.ProgressChanged -= worker_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted -= worker_ProgressCompleted;

            progressForm.Close();
            progressForm.Dispose();
            progressForm = null;
        }

        private enum ExportState : int {
            Initialization,
            Processing,
            Saving,
            Finished,
            Cancelling,
        }

        private class ExportStatus {
            public string animName;

            public int animCount;
            public int animIndex;

            public int frameCount;
            public int frameIndex;

            public int jointCount;
            public int jointIndex;
        }

        ExportProgress progressForm;

        private void ExportASET(object sender, DoWorkEventArgs e) {
            BackgroundWorker worker = (sender as BackgroundWorker);
            ExportThisOne[] items = (e.Argument as ExportThisOne[]);

            var wannaReport = new WannaReport {
                ReportProgress = (a, b) => worker.ReportProgress((int)a, b),
                CancellationPending = () => worker.CancellationPending,
                Cancel = () => e.Cancel = true,
            };

            var exportCombined = !_arg.ShouldExportIndividualClips;
            if (exportCombined) {
                ExportASETInner(wannaReport, items, false, false, false, 0, null);
                return;
            }

            ExportThisOne[] singleItem = new ExportThisOne[1];
            for (int i = 0; i < items.Length; i++) {
                singleItem[0] = items[i];
                ExportASETInner(wannaReport, singleItem, true, true, _arg.IkBones, i, items.Length);
            }
        }

        private class ExportThisOne {
            public string Text { get; set; }
            public MotionDesc MotionDesc { get; set; }
        }

        private class WannaReport {
            public Action<ExportState, ExportStatus> ReportProgress { get; set; }
            public Func<bool> CancellationPending { get; set; }
            public Action Cancel { get; set; }
        }

        private void ExportASETInner(
            WannaReport worker,
            IEnumerable<ExportThisOne> lvc,
            bool useClipNameForFileName,
            bool exportJson,
            bool includeIk,
            int baseExportIndex,
            int? numExportItems) {

            ExportStatus progress = new ExportStatus();

            // get main mesh
            OneModel m = this._models[0];

            if (m == null || m.mdlx == null || m.mset == null || !lvc.Any()) {
                return;
            }

            var t = m.mdlx.alt31[0];

            if (includeIk || exportJson) {
                // need to retrieve actual exportRootMotion
                this.CalcBody(m.triangles, m, lvc.First().MotionDesc.oneMotion, 0, UpdateFlags.Motion);
            }

            var exportRootMotion = (lvc.Count() == 1)
                ? m.exportRootMotion
                : null;

            includeIk &= exportRootMotion != null;

            // Create a new memory stream
            MemoryStream mat_data = new MemoryStream();
            BinaryWriter mat_writer = new BinaryWriter(mat_data);
            int max_ticks = 1;
            int bone_count = includeIk
                ? exportRootMotion.NumFk + exportRootMotion.NumIk
                : t.boneList.bones.Count;
            int anim_count = lvc.Count();

            progress.animCount = numExportItems.HasValue ? numExportItems.Value : anim_count;
            progress.jointCount = bone_count;

            long file_start = mat_writer.BaseStream.Position;

            mat_writer.Write(("ASET").ToCharArray(0, 4)); // bone count (4 bytes)
            mat_writer.Write((int)0); // padding (4 bytes)
            mat_writer.Write(bone_count); // bone count (4 bytes)
            mat_writer.Write(anim_count); // animation count (4 bytes)

            // get start of animation offset array
            long offsets_start = mat_writer.BaseStream.Position;

            // move past animation offset array
            mat_writer.BaseStream.Position += ((anim_count * 4) + (0x0F)) & (~0x0F);

            worker.ReportProgress(ExportState.Initialization, progress);

            if (worker.CancellationPending()) {
                worker.Cancel();
                return;
            }

            int anim_num = 0;
            // Get the frame count (ticks)
            foreach (var lvi in lvc) {
                string anim_name = lvi.Text;

                progress.animName = anim_name;
                progress.animIndex = baseExportIndex + anim_num;

                anim_name = anim_name.Replace('#', '_');

                // get motion info
                var mi = lvi.MotionDesc;
                max_ticks = (int)mi.maxTick;

                progress.frameIndex = 0;
                progress.frameCount = max_ticks;

                worker.ReportProgress(ExportState.Processing, progress);

                if (worker.CancellationPending()) {
                    worker.Cancel();
                    return;
                }

                // animation data position
                long anim_start = mat_writer.BaseStream.Position;

                // data offset array position
                mat_writer.BaseStream.Position = offsets_start + (anim_num * 4);

                // write animation data offset
                mat_writer.Write((int)anim_start); // bone count (4 bytes)

                // reset animation data position
                mat_writer.BaseStream.Position = anim_start;

                // write animation header
                mat_writer.Write((int)anim_num); // animation index (4 bytes)
                mat_writer.Write((int)max_ticks); // frame (tick) count (4 bytes)
                mat_writer.Write((int)0); // padding (4 bytes)
                mat_writer.Write((int)0); // padding (4 bytes)

                // write animation name
                mat_writer.Write(anim_name.ToCharArray(0, anim_name.Length));

                // move to matrix data start
                mat_writer.BaseStream.Position += (32 - anim_name.Length);

                // increment animation index
                ++anim_num;

                // output all the matrix transforms for each frame
                for (int i = 0; i < max_ticks; i++) {
                    progress.frameIndex = i;
                    progress.jointIndex = 0;

                    worker.ReportProgress(ExportState.Processing, progress);

                    if (worker.CancellationPending()) {
                        worker.Cancel();
                        return;
                    }

                    // set current tick
                    //tick = i;
                    // this tell his viewer to calculate the animation for the frame
                    this.CalcBody(m.triangles, m, mi.oneMotion, i, UpdateFlags.Motion);

                    // output each matrix for each bone (matrix4x4 * 4 bytes)

                    for (int bn = 0; bn < bone_count; ++bn) {
                        progress.jointIndex = bn;

                        //worker.ReportProgress((int)ExportState.Processing, progress);

                        if (worker.CancellationPending()) {
                            worker.Cancel();
                            return;
                        }

                        Matrix mat = (includeIk ? m.fkIkMatrixList : m.matrixList)[bn];
                        /* Matrix Format
                           [M11][M12][M13][M14]
                           [M21][M22][M23][M24]
                           [M31][M32][M33][M34]
                           [M41][M42][M43][M44]
                        */
                        mat_writer.Write((float)mat.M11); mat_writer.Write((float)mat.M12); mat_writer.Write((float)mat.M13);
                        mat_writer.Write((float)mat.M14);

                        mat_writer.Write((float)mat.M21); mat_writer.Write((float)mat.M22); mat_writer.Write((float)mat.M23);
                        mat_writer.Write((float)mat.M24);

                        mat_writer.Write((float)mat.M31); mat_writer.Write((float)mat.M32); mat_writer.Write((float)mat.M33);
                        mat_writer.Write((float)mat.M34);

                        mat_writer.Write((float)mat.M41); mat_writer.Write((float)mat.M42); mat_writer.Write((float)mat.M43);
                        mat_writer.Write((float)mat.M44);
                    }

                    worker.ReportProgress(ExportState.Processing, progress);
                }

                worker.ReportProgress(ExportState.Processing, progress);
            }

            worker.ReportProgress(ExportState.Processing, progress);

            // reset animation data position
            mat_writer.BaseStream.Position = 0x0C;
            mat_writer.Write((int)anim_num);

            if (worker.CancellationPending()) {
                worker.Cancel();
                return;
            }

            // Generate the file name
            string filename = useClipNameForFileName
                ? CreateFileNameForClip(lvc.First(), m)
                : m.mset.motionID;

            var asetFile = Path.ChangeExtension(filename, ".ASET");

            // open file
            FileStream outfile = File.Open(asetFile, FileMode.Create, FileAccess.ReadWrite);

            worker.ReportProgress(ExportState.Saving, progress);

            // Output all the bytes to a file
            byte[] b_data = mat_data.ToArray();

            outfile.Write(b_data, 0, b_data.Length);

            outfile.Close();

            if (exportJson && exportRootMotion != null) {
                var jsonFile = Path.ChangeExtension(filename, ".json");

                File.WriteAllText(
                    jsonFile,
                    JsonConvert.SerializeObject(
                        exportRootMotion,
                        Formatting.Indented
                    )
                );
            }

            FKBonesTweaker tweaker;
            if (includeIk && exportRootMotion != null) {
                var numBones = exportRootMotion.NumFk + exportRootMotion.NumIk;
                tweaker = new FKBonesTweaker {
                    NumBones = numBones,
                    TweakFKBones = input => {
                        if (input.Length != 64 * exportRootMotion.NumFk) {
                            throw new InvalidDataException();
                        }

                        var output = new byte[64 * numBones];
                        input.CopyTo(output);

                        foreach (var bone in exportRootMotion.FullBones.Skip(exportRootMotion.NumFk)) {
                            BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * bone.Index + 0, 4), bone.Index);
                            BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * bone.Index + 4, 4), bone.Parent);
                            BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * bone.Index + 16, 4), 0x3f800000); // Sx = 1.0f
                            BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * bone.Index + 20, 4), 0x3f800000); // Sy = 1.0f
                            BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * bone.Index + 24, 4), 0x3f800000); // Sz = 1.0f
                        }

                        return output;
                    },
                };
            }
            else {
                tweaker = null;
            }

            if (_arg.TryFindingNoesisExecutable(out var noesisExecutablePath)) {
                ConvertToFBXUsingNoesis(m, asetFile, noesisExecutablePath, tweaker);
            }
        }

        private static string CreateFileNameForClip(ExportThisOne item, OneModel model) {
            var motion = item.MotionDesc.oneMotion;
            var filename = motion.label.Replace('#', '_');
            if (FindPrecedeingMotionNameRepeats(model, motion, out var repeatedTimes))
                filename += $"_{repeatedTimes + 1}";
            filename += ".MSET";
            return filename;
        }

        private static bool FindPrecedeingMotionNameRepeats(OneModel model, OneMotion motion, out int repeatedTimes) {
            repeatedTimes = 0;
            foreach (var otherMotion in model.mset.motionList) {
                if (otherMotion == motion)
                    break;
                if (otherMotion.label == motion.label)
                    repeatedTimes++;
            }
            return repeatedTimes > 0;
        }

        private class FKBonesTweaker {
            /// <summary>
            /// FK+IK
            /// </summary>
            public int NumBones { get; set; }

            public Func<ReadOnlyMemory<byte>, ReadOnlyMemory<byte>> TweakFKBones { get; set; }
        }

        private void ConvertToFBXUsingNoesis(OneModel model, string filePath, string noesisExecutablePath, FKBonesTweaker tweaker) {
            var showConsole = _arg.ShouldShowNoesisConsole;

            //noesis needs the MDLX and ASET file names to match in order to support an export, so let's create them temporarily...
            var tempASETPath = filePath.Replace(".ASET", "_NoesisTemp.ASET");
            var tempMDLXPath = filePath.Replace(".ASET", "_NoesisTemp.MDLX");
            File.Copy(filePath, tempASETPath, true);
            File.WriteAllBytes(tempMDLXPath, model.binMdlx);

            if (tweaker != null) {
                var mdlxBody = File.ReadAllBytes(tempMDLXPath);
                var barEntries = _memoryBar.Read(mdlxBody).ToArray();
                if (barEntries.SingleOrDefault(it => it.Kind == 4) is MemoryBar.Entry modelEntry) {
                    modelEntry.Content = _petitMdlxTweaker.ReplaceFKBonesWith(
                        modelEntry.Content,
                        tweaker.NumBones,
                        tweaker.TweakFKBones(
                            _petitMdlxTweaker.AcquireFKBones(modelEntry.Content)
                        )
                    );
                    File.WriteAllBytes(tempMDLXPath, _memoryBar.Save(barEntries));
                }
            }


            //run noesis process
            var fbxPath = Path.ChangeExtension(filePath, ".fbx");
            using (var process = Process.Start(new ProcessStartInfo() {
                FileName = "cmd.exe",
                CreateNoWindow = !showConsole,
                WindowStyle = showConsole ? ProcessWindowStyle.Normal : ProcessWindowStyle.Hidden,
                Arguments = $"/{(showConsole ? 'k' : 'c')} {TryEscapingString(new FileInfo(noesisExecutablePath).FullName)} ?cmode {TryEscapingString(new FileInfo(tempMDLXPath).FullName)} {TryEscapingString(Path.GetFullPath(fbxPath))}",
            }))
                process.WaitForExit();

            //clear temp files
            if (File.Exists(fbxPath)) {
                File.Delete(tempMDLXPath);
                File.Delete(tempASETPath);
            }
        }

        private string TryEscapingString(string path) {
            return TryEscapingStringInner(path.Replace("\r", "\\r").Replace("\n", "\\n").Replace(@"\\\", @"\\"));
        }
        public static string TryEscapingStringInner(string text) {
            if (text.StartsWith("\"") && text.EndsWith("\""))
                return text;
            return text.Contains(" ") ? string.Format("\"{0}\"", text) : text;
        }

        private void buttonExportAsetMore_Click(object sender, EventArgs e) {
            using (var form = new ExportAsetOptionsForm(_arg, _saveConfig)) {
                if (form.ShowDialog(this) == DialogResult.OK) {
                    buttonExportAset_Click(sender, e);
                }
            }
        }
    }
}
