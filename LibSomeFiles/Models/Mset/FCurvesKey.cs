using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class FCurvesKey {
        public int TimeAndType { get; set; }
        public int ValueIndex { get; set; }
        public int TangentIndexLeft { get; set; }
        public int TangentIndexRight { get; set; }

        public static FCurvesKey ReadFrom(BinaryReader reader) {
            var it = new FCurvesKey();
            it.TimeAndType = reader.ReadUInt16();
            it.ValueIndex = reader.ReadUInt16();
            it.TangentIndexLeft = reader.ReadUInt16();
            it.TangentIndexRight = reader.ReadUInt16();

            return it;
        }

        public override string ToString() {
            return string.Format("{0:X4} {1:X4} {2:X4} {3:X4}", TimeAndType, ValueIndex, TangentIndexLeft, TangentIndexRight);
        }
    }
}
