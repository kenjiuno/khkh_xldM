using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LibMemoryBar {
    public class MemoryBar {
        public class Entry {
            public int Kind { get; set; }
            public string Name { get; set; }
            public ReadOnlyMemory<byte> Content { get; set; }
        }

        private readonly Encoding _raw = Encoding.GetEncoding("latin1");

        public byte[] Save(IEnumerable<Entry> entries) {
            var offsets = new List<int>();
            var nextOffset = 0;
            foreach (var entry in entries) {
                int fileAlign;
                if (entry.Kind == 7) {
                    fileAlign = 127;
                }
                else {
                    fileAlign = 3;
                }
                nextOffset = (nextOffset + fileAlign) & ~fileAlign;
                offsets.Add(nextOffset);
                nextOffset += entry.Content.Length;
            }
            var num = offsets.Count;
            var offsetTopContent = 16 + 16 * num;
            var body = new byte[offsetTopContent + nextOffset];
            var stream = new MemoryStream(body, true);
            var writer = new BinaryWriter(stream);
            writer.Write(_raw.GetBytes("BAR\x0001"));
            writer.Write(num);
            stream.Position = 16;
            foreach (var (entry, index) in entries.Select((entry, index) => (entry, index))) {
                writer.Write(entry.Kind);
                writer.Write(_raw.GetBytes(entry.Name + "\0\0\0\0"), 0, 4);
                writer.Write(offsetTopContent + offsets[index]);
                writer.Write(entry.Content.Length);
            }
            foreach (var (entry, index) in entries.Select((entry, index) => (entry, index))) {
                entry.Content.CopyTo(body.AsMemory(offsetTopContent + offsets[index], entry.Content.Length));
            }
            return body;
        }

        private class LayoutEntry {
            public int Offset { get; set; }
            public int Length { get; set; }
        }

        public IEnumerable<Entry> Read(ReadOnlyMemory<byte> body) {
            if (body.Length < 16 || _raw.GetString(body.Slice(0, 4).ToArray()) != "BAR\x0001") {
                throw new InvalidDataException();
            }
            else {
                var cy = BinaryPrimitives.ReadInt32LittleEndian(body.Slice(4, 4).Span);
                for (int y = 0; y < cy; y++) {
                    var entryHead = body.Slice(16 * (1 + y), 16);
                    var kind = BinaryPrimitives.ReadInt32LittleEndian(entryHead.Slice(0, 4).Span);
                    var name = _raw.GetString(entryHead.Slice(4, 4).ToArray());
                    var offset = BinaryPrimitives.ReadInt32LittleEndian(entryHead.Slice(8, 4).Span);
                    var length = BinaryPrimitives.ReadInt32LittleEndian(entryHead.Slice(12, 4).Span);
                    yield return new Entry {
                        Kind = kind,
                        Name = name,
                        Content = body.Slice(offset, length),
                    };
                }
            }
        }
    }
}
