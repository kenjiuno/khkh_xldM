using LibMemoryBar;
using NUnit.Framework;
using System;
using System.Buffers.Binary;
using System.IO;
using System.Linq;

namespace LibPetitMdlxTweaker.Tests {
    public class Class1 {
        private readonly MemoryBar _memoryBar = new MemoryBar();
        private readonly PetitMdlxTweaker _petitMdlxTweaker = new PetitMdlxTweaker();
        private readonly int _numFk = 228;
        private readonly int _numIk = 16;

        [Test]
        [Ignore("TDD")]
        public void P_EX100() {
            var barEntries = _memoryBar.Read(File.ReadAllBytes(@"H:\KH2fm.OpenKH\obj\P_EX100.mdlx")).ToArray();
            if (barEntries.SingleOrDefault(it => it.Kind == 4) is MemoryBar.Entry modelEntry) {
                modelEntry.Content = _petitMdlxTweaker.ReplaceFKBonesWith(
                    modelEntry.Content,
                    _numFk + _numIk,
                    TweakFKBones(
                        _petitMdlxTweaker.AcquireFKBones(modelEntry.Content),
                        _numFk + _numIk
                    )
                );
                File.WriteAllBytes(@"C:\A\P_EX100.mdlx", _memoryBar.Save(barEntries));
            }

        }

        private Memory<byte> TweakFKBones(ReadOnlyMemory<byte> input, int numBones) {
            if (input.Length != 64 * _numFk) {
                throw new InvalidDataException();
            }

            var output = new byte[64 * numBones];
            input.CopyTo(output);

            for (int index = _numFk; index < _numFk + _numIk; index++) {
                BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * index + 0, 4), index);
                BinaryPrimitives.WriteInt32LittleEndian(output.AsSpan(64 * index + 4, 4), -1);
            }

            return output;
        }
    }
}