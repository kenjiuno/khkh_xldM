#define AllowRec1

using ee1Dec;
using ee1Dec.C;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ef1Declib3 {
    internal class MobileRecompiler {
        public static bool Recompile(uint addr, IDictionary<uint, MobUt.Tx8> fragments, CustEE ee, byte[] programRam = null) {
#if AllowRec1
            string dllDir = AppDomain.CurrentDomain.BaseDirectory;
            string dllFile = Myrec.Getflib(addr, dllDir);
            if (!File.Exists(dllFile) && programRam != null) {
                Myrec.Privrec1(addr, new MemoryStream(programRam, false), dllDir);
                if (!File.Exists(dllFile)) {
                    return false;
                }
            }

            var asm = Assembly.LoadFile(dllFile);
            var classType = asm.GetType("ee1Dec.C.Class1");
            var instance = Activator.CreateInstance(classType, ee);
            var method = classType.GetMethod(LabUt.addr2Funct(addr));
            var fragment = (MobUt.Tx8)Delegate.CreateDelegate(typeof(MobUt.Tx8), instance, method);
            fragments[addr] = fragment;
            return true;
#else
            return false;
#endif
        }
    }
}
