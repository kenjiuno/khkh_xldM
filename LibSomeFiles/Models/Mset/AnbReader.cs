using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class AnbReader {
        public AnbModel Motion { get; set; } = new AnbModel();
        public int NumFKBones { get; set; }
        public int NumFKIKBones { get; set; }

        public AnbReader(Stream si) {
            var posTbl = new PosTbl(si);
            int baseoff = 0;
            int tbloff = posTbl.tbloff;

            NumFKBones = posTbl.va0;
            NumFKIKBones = posTbl.va2;

            var br = new BinaryReader(si);

            int cnt9 = 0, cnt10 = 0, cnt12 = 0;
            if (true) { // cnt9
                si.Position = baseoff + tbloff + posTbl.vc0 - baseoff; // t2
                // FCurves FK
                for (int i2 = 0; i2 < posTbl.vc4; i2++) {
                    br.ReadByte();
                    br.ReadByte();
                    br.ReadByte();
                    int tcx = br.ReadByte();
                    int tx = br.ReadUInt16();
                    cnt9 = Math.Max(cnt9, tx + tcx);
                }
                // FCurves IK
                si.Position = baseoff + tbloff + posTbl.vc8 - baseoff; // t2x
                for (int i2 = 0; i2 < posTbl.vcc; i2++) {
                    br.ReadByte();
                    br.ReadByte();
                    br.ReadByte();
                    int tcx = br.ReadByte();
                    int tx = br.ReadUInt16();
                    cnt9 = Math.Max(cnt9, tx + tcx);
                }

                if (true) { // cnt10, cnt12
                    si.Position = baseoff + tbloff + posTbl.vd0 - baseoff; // t9
                    for (int i9 = 0; i9 < cnt9; i9++) {
                        br.ReadUInt16();
                        int ti10 = br.ReadUInt16(); cnt10 = Math.Max(cnt10, ti10 + 1);
                        int ti12a = br.ReadUInt16(); cnt12 = Math.Max(cnt12, ti12a + 1);
                        int ti12b = br.ReadUInt16(); cnt12 = Math.Max(cnt12, ti12b + 1);
                    }
                }
            }
            int cntt8 = 0;
            if (true) {
                si.Position = baseoff + tbloff + posTbl.ve0 - baseoff; // t3
                for (int i3 = 0; i3 < posTbl.ve4; i3++) {
                    br.ReadUInt16();
                    br.ReadUInt16();
                    br.ReadUInt16();
                    int ti8 = br.ReadInt16(); cntt8 = Math.Max(cntt8, ti8 + 1);
                    br.ReadUInt16();
                    br.ReadUInt16();
                }
            }

            int off1 = tbloff + posTbl.vb4; int cnt1 = posTbl.vb8;
            si.Position = off1;
            for (int a1 = 0; a1 < cnt1; a1++) {
                int c00 = br.ReadUInt16();
                int c02 = br.ReadUInt16();
                float c04 = br.ReadSingle();
                Motion.InitialPositionList.Add(new InitialPosition(c00, c02, c04));
            }

            int off10 = tbloff + posTbl.vd8;
            si.Position = off10;
            Motion.KeyValueList.Capacity = cnt10;
            for (int a = 0; a < cnt10; a++) {
                Motion.KeyValueList.Add(br.ReadSingle());
            }

            int off11 = tbloff + posTbl.vd4;
            int cnt11 = posTbl.vb0;
            si.Position = off11;
            Motion.FrameTimeList.Capacity = cnt11;
            for (int a = 0; a < cnt11; a++) {
                Motion.FrameTimeList.Add(br.ReadSingle());
            }

            int off12 = tbloff + posTbl.vdc;
            si.Position = off12;
            Motion.TangentValueList.Capacity = cnt12;
            for (int a = 0; a < cnt12; a++) {
                Motion.TangentValueList.Add(br.ReadSingle());
            }

            int off9 = tbloff + posTbl.vd0;
            si.Position = off9;
            for (int a = 0; a < cnt9; a++) {
                Motion.FCurvesKeysList.Add(FCurvesKey.ReadFrom(br));
            }

            // FCurves FK
            int off2 = tbloff + posTbl.vc0;
            int cnt2 = posTbl.vc4;
            si.Position = off2;
            for (int a = 0; a < cnt2; a++) {
                Motion.FCurvesFKList.Add(FCurves.ReadFrom(br));
            }
            for (int a = 0; a < cnt2; a++) {
                FCurves o2 = Motion.FCurvesFKList[a];
                for (int b = 0; b < o2.KeyCount; b++) {
                    FCurvesKey o9 = Motion.FCurvesKeysList[o2.KeyStartIndex + b];

                    int t9c00 = o9.TimeAndType; // t11_xxxx
                    int t9c02 = o9.ValueIndex; // t10_xxxx
                    int t9c04 = o9.TangentIndexLeft; // t12_xxxx
                    int t9c06 = o9.TangentIndexRight; // t12_xxxx

                    o2.Keys.Add(new FCurvesKeyImmediate {
                        AbsKeyStartIndex = o2.KeyStartIndex + b,
                        FrameTime = Motion.FrameTimeList[t9c00 >> 2],
                        KeyValue = Motion.KeyValueList[t9c02],
                        TangentLeft = Motion.TangentValueList[t9c04],
                        TangentRight = Motion.TangentValueList[t9c06],
                    });
                }
            }

            // FCurves IK
            int off2x = tbloff + posTbl.vc8;
            int cnt2x = posTbl.vcc;
            si.Position = off2x;
            for (int a = 0; a < cnt2x; a++) {
                Motion.FCurvesIKList.Add(FCurves.ReadFrom(br));
            }
            for (int a = 0; a < cnt2x; a++) {
                FCurves o2 = Motion.FCurvesIKList[a];
                for (int b = 0; b < o2.KeyCount; b++) {
                    FCurvesKey o9 = Motion.FCurvesKeysList[o2.KeyStartIndex + b];

                    int t9c00 = o9.TimeAndType; // t11_xxxx
                    int t9c02 = o9.ValueIndex; // t10_xxxx
                    int t9c04 = o9.TangentIndexLeft; // t12_xxxx
                    int t9c06 = o9.TangentIndexRight; // t12_xxxx

                    o2.Keys.Add(new FCurvesKeyImmediate {
                        AbsKeyStartIndex = o2.KeyStartIndex + b,
                        FrameTime = Motion.FrameTimeList[t9c00 >> 2],
                        KeyValue = Motion.KeyValueList[t9c02],
                        TangentLeft = Motion.TangentValueList[t9c04],
                        TangentRight = Motion.TangentValueList[t9c06],
                    });
                }
            }

            int off3 = tbloff + posTbl.ve0;
            int cnt3 = posTbl.ve4;
            si.Position = off3;
            for (int a3 = 0; a3 < cnt3; a3++) {
                int c00 = br.ReadByte();
                int c01 = br.ReadByte();
                int c02 = br.ReadUInt16();
                int c04 = br.ReadUInt16();
                int c06 = br.ReadUInt16();
                uint c08 = br.ReadUInt32();
                Motion.ConstraintList.Add(new Constraint(c00, c01, c02, c04, c06, c08));
            }

            int off4 = tbloff + posTbl.vac;
            int cnt4 = posTbl.va2;
            si.Position = off4;
            for (int a4 = 0; a4 < cnt4; a4++) {
                int c00 = br.ReadUInt16();
                int c02 = br.ReadUInt16();
                Motion.JointIndexList.Add(new JointIndex(c00, c02));
            }

            si.Position = tbloff + posTbl.vbc;
            Motion.RootPosition = RootPosition.ReadFrom(br);

            Motion.OffsetIKBones = tbloff + posTbl.va8;
            Motion.NumIKBones = (posTbl.va2 - posTbl.va0);
            si.Position = Motion.OffsetIKBones;
            for (int a5 = 0; a5 < Motion.NumIKBones; a5++) {
                AxBone o = new AxBone();
                o.cur = br.ReadInt32();
                o.parent = br.ReadInt32();
                o.v08 = br.ReadInt32();
                o.v0c = br.ReadInt32();
                o.x1 = br.ReadSingle();
                o.y1 = br.ReadSingle();
                o.z1 = br.ReadSingle();
                o.w1 = br.ReadSingle();
                o.x2 = br.ReadSingle();
                o.y2 = br.ReadSingle();
                o.z2 = br.ReadSingle();
                o.w2 = br.ReadSingle();
                o.x3 = br.ReadSingle();
                o.y3 = br.ReadSingle();
                o.z3 = br.ReadSingle();
                o.w3 = br.ReadSingle();
                Motion.IKBones.Add(o);
            }
        }
    }
}
