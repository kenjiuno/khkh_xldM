npx bit-field -i map1.json --bits 128 --lanes 4 --hflip > map1.svg
npx bit-field -i flags1.json --bits 16 --lanes 1 --hflip > flags1.svg
npx bit-field -i flags2.json --bits 8 --lanes 1 --hflip > flags2.svg
npx bit-field -i flags3.json --bits 8 --lanes 1 --hflip > flags3.svg
