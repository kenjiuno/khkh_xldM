using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace khkh_xldMii.Models.Mset {
    public class RootPosition {
        public float ScalingX { get; set; }
        public float ScalingY { get; set; }
        public float ScalingZ { get; set; }
        public float RotationX { get; set; }
        public float RotationY { get; set; }
        public float RotationZ { get; set; }
        public float TranslationX { get; set; }
        public float TranslationY { get; set; }
        public float TranslationZ { get; set; }
        public int[] FCurves { get; set; } = new int[9];

        public static RootPosition ReadFrom(BinaryReader reader) {
            var it = new RootPosition();
            it.ScalingX = reader.ReadSingle();
            it.ScalingY = reader.ReadSingle();
            it.ScalingZ = reader.ReadSingle();
            reader.ReadSingle();

            it.RotationX = reader.ReadSingle();
            it.RotationY = reader.ReadSingle();
            it.RotationZ = reader.ReadSingle();
            reader.ReadSingle();

            it.TranslationX = reader.ReadSingle();
            it.TranslationY = reader.ReadSingle();
            it.TranslationZ = reader.ReadSingle();
            reader.ReadSingle();

            for (int x = 0; x < 9; x++) {
                it.FCurves[x] = reader.ReadInt32();
            }

            return it;
        }
    }
}
