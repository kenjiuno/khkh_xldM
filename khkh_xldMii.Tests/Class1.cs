using ef1Declib;
using khkh_xldMii.Helpers;
using khkh_xldMii.Models.Mset;
using khkh_xldMii.Utils.Mset;
using NUnit.Framework;
using SlimDX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace khkh_xldMii.Tests {
    public class Class1 {
        private readonly MobileMatrixComputer2 _emuRunner = new MobileMatrixComputer2();

        [Test]
        [TestCaseSource(nameof(GetSources))]
        public void MassiveTests(string mdlxFile, string msetFile, int anbOff, float minTick, float maxTick, int numMatrices) {
            var binMdlx = File.ReadAllBytes(mdlxFile);
            var binMset = File.ReadAllBytes(msetFile);

            _emuRunner.Load(
                new MemoryStream(binMdlx, false),
                new MemoryStream(binMset, false)
            );
            maxTick = Math.Min(maxTick, 500);
            for (float tick = minTick; tick <= maxTick; tick += 1.0f) {
                var result = _emuRunner.Compute(anbOff, tick, numMatrices);
                //Helpers.MatrixExtractor.ExtractMatricesFrom(result.Matrices, numMatrices);
                var basePose = ExtractMatricesFrom(result.Matrices, numMatrices);
            }
        }

        private static Matrix[] ExtractMatricesFrom(byte[] buffer, int num) {
            var items = new Matrix[num];
            var offset = 0;
            for (int idx = 0; idx < num; idx++) {
                var m = new Matrix();
                m.M11 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M12 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M13 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M14 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M21 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M22 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M23 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M24 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M31 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M32 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M33 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M34 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M41 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M42 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M43 = BitConverter.ToSingle(buffer, offset); offset += 4;
                m.M44 = BitConverter.ToSingle(buffer, offset); offset += 4;
                items[idx] = m;
            }
            return items;
        }

        private static readonly string _kh2AssetExportedDir = @"H:\KH2fm.OpenKH";

        public static IEnumerable<object[]> GetSources() {
            var fileList = new string[0]
                .Concat(Directory.GetFiles(Path.Combine(_kh2AssetExportedDir, "obj"), "B_*.mdlx"))
                .Concat(Directory.GetFiles(Path.Combine(_kh2AssetExportedDir, "obj"), "G_*.mdlx"))
                .Distinct()
                .ToArray();
            foreach (var mdlxFile in fileList) {
                var msetFile = Path.ChangeExtension(mdlxFile, ".mset");
                if (File.Exists(msetFile)) {
                    Msetfst msetfst;
                    using (var msetStream = File.OpenRead(msetFile)) {
                        msetfst = new Msetfst(msetStream, Path.GetFileName(msetFile));
                    }
                    foreach (var oneMotion in msetfst.motionList) {
                        float maxTick;
                        int numMatrices;
                        if (oneMotion.isRaw) {
                            AnbRawReader reader = new AnbRawReader(new MemoryStream(oneMotion.anbBin, false));
                            maxTick = reader.cntFrames;
                            numMatrices = reader.cntJoints;
                        }
                        else {
                            AnbReader reader = new AnbReader(new MemoryStream(oneMotion.anbBin, false));
                            maxTick = (reader.Motion.FrameTimeList.Length != 0) ? reader.Motion.FrameTimeList[reader.Motion.FrameTimeList.Length - 1] : 0;
                            numMatrices = reader.NumFKBones;
                        }
                        if (1 <= numMatrices) {
                            yield return new object[] { mdlxFile, msetFile, (int)oneMotion.anbOff, 0, maxTick, numMatrices, };
                        }
                    }
                }
            }
        }
    }
}
