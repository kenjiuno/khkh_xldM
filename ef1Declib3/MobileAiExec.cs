using ef1Declib3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ee1Dec.C {
    public partial class MobileAiExec {
        private readonly byte[] _programRam;
        private const uint funcPuti = 0x1000U;
        private const uint funcPutf = 0x1001U;
        private const uint funcPuts = 0x1002U;

        public class Result {
            public BdStatus Status { get; set; }
            public string Log { get; set; }
        }

        public MobileAiExec() {
            InitState();

            using (var file = File.OpenRead(@"H:\Proj\khkh_xldM\MEMO\expSim\AiExec\MobileAiExec.bin")) {
                var size = 1024 * 1024 * 32;
                _programRam = new byte[size];
                if (file.Read(_programRam, 0, size) != size) {
                    throw new EndOfStreamException();
                }
            }
        }

        public Result RunVM(byte[] ai, int trigger) {
            int initPc = -1;
            for (int x = 0; ; x++) {
                var thisTrigger = BitConverter.ToInt32(ai, 28 + 8 * x);
                if (thisTrigger == trigger) {
                    initPc = BitConverter.ToInt32(ai, 28 + 8 * x + 4);
                    break;
                }
                if (thisTrigger == 0) {
                    throw new Exception("Trigger " + trigger + " not found!");
                }
            }

            _ee.at.UD0 = 0U;
            _ee.v0.UD0 = 0U;
            _ee.v1.UD0 = 0U;
            _ee.a0.UD0 = 0x1000000; // BD_PROCESS *state ; Somewhere
            _ee.a1.UD0 = 0x1400000; // BD_TRAP **tabs
            _ee.a2.UD0 = 0U; // DB_DEBUG_FUNC *debug_func ; a0=state, a1=flag, a2=debug_arg
            _ee.a3.UD0 = 0U; // void *debug_arg
            _ee.t0.UD0 = 0U;
            _ee.t1.UD0 = 0U;
            _ee.t2.UD0 = 0U;
            _ee.t3.UD0 = 0U;
            _ee.t4.UD0 = 0U;
            _ee.t5.UD0 = 0U;
            _ee.t6.UD0 = 0U;
            _ee.t7.UD0 = 0U;
            _ee.s0.UD0 = 0U;
            _ee.s1.UD0 = 0U;
            _ee.s2.UD0 = 0U;
            _ee.s3.UD0 = 0U;
            _ee.s4.UD0 = 0U;
            _ee.s5.UD0 = 0U;
            _ee.s6.UD0 = 0U;
            _ee.s7.UD0 = 0U;
            _ee.t8.UD0 = 0U;
            _ee.t9.UD0 = 0U;
            _ee.k0.UD0 = 0U;
            _ee.k1.UD0 = 0U;
            _ee.gp.UD0 = 0U;
            _ee.sp.UD0 = 0x2000000U;
            _ee.s8.UD0 = 0U;
            _ee.ra.UD0 = 0xFFFFFFFFU;

            {
                var ram = _ee.ram;
                for (int x = 0, cx = ram.Length; x < cx; x++) {
                    ram[x] = 0;
                }

                // Copy switch jump tables
                Buffer.BlockCopy(_programRam, 0, ram, 0, 0x400000);
            }

            {
                int tabCount = 1;
                int tabFuncCount = 10;

                for (int x = 0; x < tabCount; x++) {
                    _ee.eeWrite32(_ee.a1.UL0 + (uint)(4 * x), (uint)(_ee.a1.UL0 + 4 * tabCount + 8 * tabFuncCount * x));
                }

                void Write(int tabIndex, int tabFuncIndex, uint func, uint flags) {
                    var addr = _ee.a1.UL0 + (uint)(4 * tabCount + 8 * (tabFuncCount * tabIndex + tabFuncIndex));
                    _ee.eeWrite32(addr + 0U, func);
                    _ee.eeWrite32(addr + 4U, flags);
                }

                Write(0, 0, funcPuti, 2);
                Write(0, 1, funcPutf, 2);
                Write(0, 2, funcPuts, 1);
            }

            int initTop = (int)(_ee.a0.UL0 + 1024); // Somewhere +1KB
            int initSp = (int)(_ee.a0.UL0 + 1024 + 1024 * 1024 * 1); // Somewhere +1KB +1MB
            int initTp = (int)(_ee.a0.UL0 + 1024 + 1024 * 1024 * 2); // Somewhere +1KB +2MB
            int initWp = (int)(_ee.a0.UL0 + 1024 + 1024 * 1024 * 3); // Somewhere +1KB +3MB

            Buffer.BlockCopy(ai, 0, _ee.ram, initTop, ai.Length);

            _ee.eeWrite32(_ee.a0.UL0 + 0x00, (uint)initTop); // BD_HEADER *top
            _ee.eeWrite32(_ee.a0.UL0 + 0x04, (uint)initPc); // int pc
            _ee.eeWrite32(_ee.a0.UL0 + 0x08, (uint)initSp); // int* sp
            _ee.eeWrite32(_ee.a0.UL0 + 0x0C, (uint)initTp); // int* tp
            _ee.eeWrite32(_ee.a0.UL0 + 0x10, (uint)initWp); // void* wp

            for (int x = 0; x < 32; x++) {
                _ee.fpr[x].f = 0;
            }

            var log = new StringWriter();

            string ReadString(uint pointer) {
                pointer = _ee.eeRead32(pointer);
                var text = "";
                while (true) {
                    var one = _ee.eeRead8(pointer);
                    if (one == 0) {
                        break;
                    }
                    text += (char)one;
                    pointer++;
                }
                return text;
            }

            _ee.pc = 0x001da558;
            while (_ee.pc != 0xFFFFFFFFU) {
                if (_ee.pc == funcPuti) {
                    log.Write(ReadString(_ee.a0.UL0));
                    log.Write(_ee.eeRead32(_ee.a0.UL0 + 4));
                    _ee.pc = _ee.ra.UL0;
                }
                else if (_ee.pc == funcPutf) {
                    log.Write(ReadString(_ee.a0.UL0));
                    log.Write(BitConverter.ToSingle(BitConverter.GetBytes(_ee.eeRead32(_ee.a0.UL0 + 4)), 0));
                    _ee.pc = _ee.ra.UL0;
                }
                else if (_ee.pc == funcPuts) {
                    log.Write(ReadString(_ee.a0.UL0));
                    _ee.pc = _ee.ra.UL0;
                }
                else if (_fragments.ContainsKey(_ee.pc) || MobileRecompiler.Recompile(_ee.pc, _fragments, _ee, _programRam)) {
                    _fragments[_ee.pc]();
                }
                else {
                    throw new RecfnnotFound(_ee.pc, "AiExec");
                }
            }

            if (_ee.eeRead32(_ee.a0.UL0 + 0x08) != (uint)initSp) {
                throw new Exception("sp");
            }
            if (_ee.eeRead32(_ee.a0.UL0 + 0x0C) != (uint)initTp) {
                throw new Exception("tp");
            }

            return new Result {
                Status = (BdStatus)_ee.v0.UL0,
                Log = log.ToString(),
            };
        }
    }
}
