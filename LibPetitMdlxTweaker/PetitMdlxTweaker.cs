using System;
using System.Buffers.Binary;
using System.IO;

namespace LibPetitMdlxTweaker {
    public class PetitMdlxTweaker {
        private readonly int _top = 0x90;
        private readonly int _offsetNumFKBones = 0x10;
        private readonly int _offsetFKBones = 0x14;

        public ReadOnlyMemory<byte> AcquireFKBones(ReadOnlyMemory<byte> body) {
            if (body.Length < 0x90) {
                throw new InvalidDataException();
            }

            var numFKBones = BinaryPrimitives.ReadUInt16LittleEndian(body.Slice(_top + _offsetNumFKBones, 2).Span);
            var offsetFKBones = BinaryPrimitives.ReadInt32LittleEndian(body.Slice(_top + _offsetFKBones, 4).Span);

            return body.Slice(_top + offsetFKBones, 64 * numFKBones);
        }

        public Memory<byte> ReplaceFKBonesWith(ReadOnlyMemory<byte> content, int numNewFKBones, ReadOnlyMemory<byte> fkBones) {
            if (content.Length < 0x90 + 0x10) {
                throw new InvalidDataException("Mdlx size is too small. At least 0x90 + sizeof(ModelHeader)");
            }

            var rootType = BinaryPrimitives.ReadInt32LittleEndian(content.Slice(_top + 0, 4).Span);
            if (rootType != 3) {
                throw new InvalidDataException("Model root type must be 3 (skeltal model)");
            }

            var rootSubtype = BinaryPrimitives.ReadInt32LittleEndian(content.Slice(_top + 4, 4).Span);
            if (rootSubtype != 0) {
                throw new InvalidDataException("Model root sub type must be 0 (character obj)");
            }

            var numFKBones = BinaryPrimitives.ReadUInt16LittleEndian(content.Slice(_top + _offsetNumFKBones, 2).Span);
            var offsetFKBones = BinaryPrimitives.ReadInt32LittleEndian(content.Slice(_top + _offsetFKBones, 4).Span);

            var sizeDiff = 64 * (numNewFKBones - numFKBones);

            // Add sizeDiff to offset field value
            int ApplyRelocation(Span<byte> span) {
                var newValue = BinaryPrimitives.ReadInt32LittleEndian(span) + sizeDiff;
                BinaryPrimitives.WriteInt32LittleEndian(span, newValue);
                return newValue;
            }

            var newBodySize = content.Length + sizeDiff;
            var newBody = new byte[newBodySize];

            // rootHeader
            //  modelHeader (skeletal obj)
            //   part1
            //   part2
            //   part3
            //   partX
            //   skeletonExtra
            //   fkBones (array of matrices)
            //    <-- injectHere `sizeDiff` bytes
            //   moreExtra (VIF, DMAtag, data, and so on)
            //  next modelHeader (shadow obj)
            //   ...

            {
                var beforeFKBonesSrc = content.Slice(_top, offsetFKBones);
                var beforeFKBonesDest = newBody.AsMemory(_top, offsetFKBones);

                beforeFKBonesSrc.CopyTo(beforeFKBonesDest);
            }

            {
                // offsetNextModel
                ApplyRelocation(newBody.AsSpan(_top + 12, 4));
            }

            {
                var afterFKBonesSrc = content.Slice(_top + offsetFKBones + 64 * numFKBones, content.Length - (_top + offsetFKBones + 64 * numFKBones));
                var afterFKBonesDest = newBody.AsMemory(_top + offsetFKBones + 64 * numNewFKBones, newBody.Length - (_top + offsetFKBones + 64 * numNewFKBones));

                afterFKBonesSrc.CopyTo(afterFKBonesDest);
            }

            {
                var numParts = BinaryPrimitives.ReadInt32LittleEndian(content.Slice(_top + 0x1C, 4).Span);

                for (int y = 0; y < numParts; y++) {
                    var offsetNewDmaTag = ApplyRelocation(newBody.AsSpan(_top + 0x20 + 0x20 * y + 0x10, 4));
                    var offsetNewMatrixRef = ApplyRelocation(newBody.AsSpan(_top + 0x20 + 0x20 * y + 0x14, 4));

                    var offsetWalkingDmaTag = _top + offsetNewDmaTag;
                    var numInts = BinaryPrimitives.ReadInt32LittleEndian(newBody.AsSpan(_top + offsetNewMatrixRef, 4));
                    var spanInts = newBody.AsSpan(_top + offsetNewMatrixRef + 4, 4 * numInts);

                    // --- matrixRef ints
                    // int numInts; // 0x01B1
                    // int 0x35
                    // int 0x02
                    // int 0x18
                    // int 0x29
                    // int 0x0C
                    // int 0x31
                    // int 0x3F
                    // int 0x14
                    // int 0x22
                    // int 0x16
                    // int -1
                    // int 0x35
                    // ...

                    // --- DmaTag
                    // relocate the offsets inside marks `<` and `>`

                    // DA20h: 64 00 00 30<C0 3A 00 00>00 00 00 00 00 00 00 00  d..0.:..........  // vif transfer (need relocation)
                    // DA30h: 04 00 00 30 35 00 00 00 01 01 00 01 A2 80 04 6C  ...05..........l  // matrix transfer
                    // DA40h: 04 00 00 30 02 00 00 00 01 01 00 01 A6 80 04 6C  ...0...........l  // matrix transfer 
                    // DA50h: 04 00 00 30 18 00 00 00 01 01 00 01 AA 80 04 6C  ...0...........l  // matrix transfer
                    // DA60h: 04 00 00 30 29 00 00 00 01 01 00 01 AE 80 04 6C  ...0)..........l  // matrix transfer
                    // DA70h: 04 00 00 30 0C 00 00 00 01 01 00 01 B2 80 04 6C  ...0...........l  // matrix transfer
                    // DA80h: 04 00 00 30 31 00 00 00 01 01 00 01 B6 80 04 6C  ...01..........l  // matrix transfer
                    // DA90h: 04 00 00 30 3F 00 00 00 01 01 00 01 BA 80 04 6C  ...0?..........l  // matrix transfer
                    // DAA0h: 04 00 00 30 14 00 00 00 01 01 00 01 BE 80 04 6C  ...0...........l  // matrix transfer
                    // DAB0h: 04 00 00 30 22 00 00 00 01 01 00 01 C2 80 04 6C  ...0"..........l  // matrix transfer
                    // DAC0h: 04 00 00 30 16 00 00 00 01 01 00 01 C6 80 04 6C  ...0...........l  // matrix transfer
                    // DAD0h: 00 00 00 10 00 00 00 00 00 00 00 17 00 00 00 00  ................  // terminator of this batch
                    // DAE0h: 66 00 00 30<00 41 00 00>00 00 00 00 00 00 00 00  f..0.A..........  // vif transfer (need relocation)
                    // DAF0h: 04 00 00 30 35 00 00 00 01 01 00 01 A5 80 04 6C  ...05..........l  // matrix transfer

                    for (int x = 0; x < numInts;) {
                        ApplyRelocation(newBody.AsSpan(offsetWalkingDmaTag + 4, 4));

                        int prevX = x;
                        for (; x < numInts && BinaryPrimitives.ReadInt32LittleEndian(spanInts.Slice(4 * x, 4)) != -1;) {
                            x++;
                        }
                        x++;

                        offsetWalkingDmaTag += 16 * (x - prevX + 1);
                    }
                }
            }

            BinaryPrimitives.WriteUInt16LittleEndian(newBody.AsSpan(_top + _offsetNumFKBones, 2), Convert.ToUInt16(numNewFKBones));

            {
                var fkBonesDest = newBody.AsMemory(_top + offsetFKBones, 64 * numNewFKBones);

                fkBones.CopyTo(fkBonesDest);
            }

            return newBody;
        }
    }
}
