using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace khiiMapv.Utils {
    internal class MeasureDoctLoad {
        public MeasureDoctLoad(
            int numMeshGroupsScanned,
            int numMeshesScanned,
            int numVifPacketsProcessed,
            int numTrianglesGenerated
        ) {
            NumMeshGroupsScanned = numMeshGroupsScanned;
            NumMeshesScanned = numMeshesScanned;
            NumVifPacketsProcessed = numVifPacketsProcessed;
            NumTrianglesGenerated = numTrianglesGenerated;
        }

        public override string ToString() => $"{NumMeshGroupsScanned}, {NumMeshesScanned}, {NumVifPacketsProcessed}, {NumTrianglesGenerated}";

        public int NumMeshGroupsScanned { get; }
        public int NumMeshesScanned { get; }
        public int NumVifPacketsProcessed { get; }
        public int NumTrianglesGenerated { get; }

        public int GuessedFps => (int)(
            80
            - NumMeshGroupsScanned / 100.0
            - NumMeshesScanned / 100.0
            - NumVifPacketsProcessed / 1000.0
            - NumTrianglesGenerated / 10000.0
        );
    }
}
